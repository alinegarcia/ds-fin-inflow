-- BP - Shop in shop e Marketplace In

with ref as (
select 
  cp.company_name 									as company_name
, sum(coalesce(its.refund_product_value, 0) 
	+ coalesce(its.refund_shipping_value, 0)
	+ coalesce(its.refund_gift_wrap_value, 0))/sum(coalesce(its.total_gmv,0)) as refund_voucher_rate
from star_schema.fact_item_sold its
	left join star_schema.dim_store st on its.fk_store = st.id_store
	left join star_schema.dim_company cp on its.fk_company = cp.id_company
	left join star_schema.dim_payment_method py on its.fk_payment_method = py.id_payment_method
	left join star_schema.dim_date dt on its.fk_order_date = dt.id_date
	--left join mapping.map_company_store mcp on its.fk_company = mcp.fk_company and st.store_name = mcp.store_name
where 1=1
	and dt.date_ between (sysdate - 64) and (sysdate - 8)
	and cp.company_code in ('DBR','TRI','KAN')
	and its.is_invalid = 0
	and st.is_marketplace_out = 0 -- no marketplace out
	and (coalesce(its.total_gmv,0) - coalesce(its.refund_product_value, 0) - coalesce(its.refund_shipping_value, 0) - coalesce(its.refund_gift_wrap_value, 0)) > 0
group by
  cp.company_name), 			


pay_meth as (
select 
 cp.company_name 									as company_name
, cast(sum(case when coalesce(py.payment_method_group, 'OTHER') = 'CREDIT CARD' then coalesce(its.total_gmv,0) end)
	/sum(coalesce(its.total_gmv,0)) as double precision) as credit_card_share
, cast(sum(case when coalesce(py.payment_method_group, 'OTHER') = 'BANK SLIP' then coalesce(its.total_gmv,0) end)
	/sum(coalesce(its.total_gmv,0)) as double precision) as bank_slip_share
, cast(sum(case when coalesce(py.payment_method_group, 'OTHER') = 'ONLINE DEBIT' then coalesce(its.total_gmv,0) end)
	/sum(coalesce(its.total_gmv,0)) as double precision) as online_debit_share
, cast(sum(case when coalesce(py.payment_method_group, 'OTHER') = 'WALLET PAYMENT' then coalesce(its.total_gmv,0) end)
	/sum(coalesce(its.total_gmv,0)) as double precision) as wallet_share
, cast(sum(case when coalesce(py.payment_method_group, 'OTHER') = 'OTHER' then coalesce(its.total_gmv,0) end)
	/sum(coalesce(its.total_gmv,0)) as double precision) as other_share

from star_schema.fact_item_sold its
	left join star_schema.dim_store st on its.fk_store = st.id_store
	left join star_schema.dim_company cp on its.fk_company = cp.id_company
	left join star_schema.dim_payment_method py on its.fk_payment_method = py.id_payment_method
	left join star_schema.dim_date dt on its.fk_order_date = dt.id_date
	--left join mapping.map_company_store mcp on its.fk_company = mcp.fk_company and st.store_name = mcp.store_name
where 1=1
	and dt.date_ between (sysdate - 64) and (sysdate - 8)
	and cp.company_code in ('DBR','TRI','KAN')
	and its.is_invalid = 0
	and st.is_marketplace_out = 0 -- no marketplace out
	and coalesce(its.total_gmv,0) > 0
group by
  cp.company_name),

  
cnc as (
select
  cp.company_name 									as company_name
, 1 - sum(case when is_paid = 1 then coalesce(its.total_gmv,0)
  - coalesce(its.refund_product_value, 0) 
  - coalesce(its.refund_shipping_value, 0)
  - coalesce(its.refund_gift_wrap_value, 0) else 0 end)/
  sum(coalesce(its.total_gmv,0)
  - coalesce(its.refund_product_value, 0) 
  - coalesce(its.refund_shipping_value, 0)
  - coalesce(its.refund_gift_wrap_value, 0)) 		as cancellation_rate
, 1 - sum(case when is_paid = 1 and coalesce(py.payment_method_group, 'OTHER') = 'CREDIT CARD' then coalesce(its.total_gmv,0)
  - coalesce(its.refund_product_value, 0) 
  - coalesce(its.refund_shipping_value, 0)
  - coalesce(its.refund_gift_wrap_value, 0) else 0 end)/
  sum(case when coalesce(py.payment_method_group, 'OTHER') = 'CREDIT CARD' then coalesce(its.total_gmv,0)
  - coalesce(its.refund_product_value, 0) 
  - coalesce(its.refund_shipping_value, 0)
  - coalesce(its.refund_gift_wrap_value, 0) end) 	as cancellation_rate_credit_card
, 1 - sum(case when is_paid = 1 and coalesce(py.payment_method_group, 'OTHER') = 'BANK SLIP' then coalesce(its.total_gmv,0)
  - coalesce(its.refund_product_value, 0) 
  - coalesce(its.refund_shipping_value, 0)
  - coalesce(its.refund_gift_wrap_value, 0) else 0 end)/
  sum(case when coalesce(py.payment_method_group, 'OTHER') = 'BANK SLIP' then coalesce(its.total_gmv,0)
  - coalesce(its.refund_product_value, 0) 
  - coalesce(its.refund_shipping_value, 0)
  - coalesce(its.refund_gift_wrap_value, 0) end) 	as cancellation_rate_bank_slip
  , 1 - sum(case when is_paid = 1 and coalesce(py.payment_method_group, 'OTHER') = 'ONLINE DEBIT' then coalesce(its.total_gmv,0)
  - coalesce(its.refund_product_value, 0) 
  - coalesce(its.refund_shipping_value, 0)
  - coalesce(its.refund_gift_wrap_value, 0) else 0 end)/
  sum(case when coalesce(py.payment_method_group, 'OTHER') = 'ONLINE DEBIT' then coalesce(its.total_gmv,0)
  - coalesce(its.refund_product_value, 0) 
  - coalesce(its.refund_shipping_value, 0)
  - coalesce(its.refund_gift_wrap_value, 0) end) 	as cancellation_rate_online_debit
  , 1 - sum(case when is_paid = 1 and coalesce(py.payment_method_group, 'OTHER') = 'WALLET PAYMENT' then coalesce(its.total_gmv,0)
  - coalesce(its.refund_product_value, 0) 
  - coalesce(its.refund_shipping_value, 0)
  - coalesce(its.refund_gift_wrap_value, 0) else 0 end)/
  sum(case when coalesce(py.payment_method_group, 'OTHER') = 'WALLET PAYMENT' then coalesce(its.total_gmv,0)
  - coalesce(its.refund_product_value, 0) 
  - coalesce(its.refund_shipping_value, 0)
  - coalesce(its.refund_gift_wrap_value, 0) end) 	as cancellation_rate_wallet
, 1 - sum(case when is_paid = 1 and coalesce(py.payment_method_group, 'OTHER') = 'OTHER' then coalesce(its.total_gmv,0)
  - coalesce(its.refund_product_value, 0) 
  - coalesce(its.refund_shipping_value, 0)
  - coalesce(its.refund_gift_wrap_value, 0) else 0 end)/
  sum(case when coalesce(py.payment_method_group, 'OTHER') = 'OTHER' then coalesce(its.total_gmv,0)
  - coalesce(its.refund_product_value, 0) 
  - coalesce(its.refund_shipping_value, 0)
  - coalesce(its.refund_gift_wrap_value, 0) end) 	as cancellation_rate_other

from star_schema.fact_item_sold its
	left join star_schema.dim_store st 			on its.fk_store = st.id_store
	left join star_schema.dim_company cp 		on its.fk_company = cp.id_company
	left join star_schema.dim_payment_method py on its.fk_payment_method = py.id_payment_method
	left join star_schema.dim_date dt 			on its.fk_order_date = dt.id_date
	--left join mapping.map_company_store mcp 	on its.fk_company = mcp.fk_company and st.store_name = mcp.store_name
where 1=1
	and dt.date_ between (sysdate - 64) and (sysdate - 8)
	and cp.company_code in ('DBR','TRI','KAN')
	and its.is_invalid = 0
	and st.is_marketplace_out = 0 -- no marketplace out
	and coalesce(its.total_gmv,0) > 0
group by
  cp.company_name),
  
  
  
  
bank_slip_inflow as (
select 
	aux1.company_name, aux1.payment_method_group, aux1.order_weekday, aux1.customer_payment_leadtime_days, 
	aux1.sample_qty, aux2.sample_total_qty, (aux1.sample_qty/aux2.sample_total_qty::float) expected_payment_share
from
	(
	select 
	  dt.day_of_week  								as order_weekday
	, cp.company_name 								as company_name
	, py.payment_method_group						as payment_method_group
	, datediff(day, its.order_date, its.paid_date) 	as customer_payment_leadtime_days
	, count(distinct(order_number)) 				as sample_qty
	from star_schema.fact_item_sold its
		left join star_schema.dim_company cp 		on its.fk_company = cp.id_company
		left join star_schema.dim_store st 			on st.fk_company = cp.id_company
		left join star_schema.dim_payment_method py on its.fk_payment_method = py.id_payment_method
		left join star_schema.dim_date dt 			on its.fk_order_date = dt.id_date
	where 1=1
		and st.is_marketplace_out = 0
		and dt.date_ between (sysdate - 64) and (sysdate - 8) --
		and paid_date is not null
		and py.payment_method_group = 'BANK SLIP'
		and cp.company_code in ('DBR','TRI','KAN')
		and datediff(day, its.order_date, its.paid_date) between 0 and 8 -- max relevant leadtime (>=0.9%)
	group by
	  dt.day_of_week 
	, cp.company_name 							
	, py.payment_method_group 					
	, datediff(day, its.order_date, its.paid_date)
	) aux1
join 
	(
	select 
	  dt.day_of_week  								as order_weekday
	, cp.company_name 								as company_name
	, py.payment_method_group						as payment_method_group
	-- , datediff(day, its.order_date, its.paid_date) 	as processing_days
	, count(distinct(order_number))					as sample_total_qty
	from star_schema.fact_item_sold its
		left join star_schema.dim_company cp 		on its.fk_company = cp.id_company
		left join star_schema.dim_store st 			on st.fk_company = cp.id_company
		left join star_schema.dim_payment_method py on its.fk_payment_method = py.id_payment_method
		left join star_schema.dim_date dt 			on its.fk_order_date = dt.id_date
	where 1=1
		and st.is_marketplace_out = 0
		and dt.date_ between (sysdate - 64) and (sysdate - 8)
		and paid_date is not null
		and py.payment_method_group = 'BANK SLIP'
		and cp.company_code in ('DBR','TRI','KAN')
		and datediff(day, its.order_date, its.paid_date) between 0 and 8 
	group by
	  dt.day_of_week 
	, cp.company_name 							
	, py.payment_method_group 					
	-- , datediff(day, its.order_date, its.paid_date)
	) aux2   on aux1.order_weekday = aux2.order_weekday
				and aux1.company_name = aux2.company_name
				and aux1.payment_method_group = aux2.payment_method_group
),

credit_card_inflow as (
select 
	aux1.company_name, aux1.payment_method_group, aux1.order_weekday, aux1.customer_payment_leadtime_days, 
	aux1.sample_qty, aux2.sample_total_qty, (aux1.sample_qty/aux2.sample_total_qty::float) expected_payment_share
from
	(
	select 
	  dt.day_of_week  								as order_weekday
	, cp.company_name 								as company_name
	, py.payment_method_group						as payment_method_group
	, datediff(day, its.order_date, its.paid_date) 	as customer_payment_leadtime_days
	, count(distinct(order_number)) 				as sample_qty
	from star_schema.fact_item_sold its
		left join star_schema.dim_company cp 		on its.fk_company = cp.id_company
		left join star_schema.dim_store st 			on st.fk_company = cp.id_company
		left join star_schema.dim_payment_method py on its.fk_payment_method = py.id_payment_method
		left join star_schema.dim_date dt 			on its.fk_order_date = dt.id_date
	where 1=1
		and st.is_marketplace_out = 0
		and dt.date_ between (sysdate - 64) and (sysdate - 8)
		and paid_date is not null
		and py.payment_method_group = 'CREDIT CARD'
		and cp.company_code in ('DBR','TRI','KAN')
		and datediff(day, its.order_date, its.paid_date) between 0 and 2 -- max relevant leadtime (>=0.9%)
	group by
	  dt.day_of_week 
	, cp.company_name 							
	, py.payment_method_group 					
	, datediff(day, its.order_date, its.paid_date)
	) aux1
join 
	(
	select 
	  dt.day_of_week  								as order_weekday
	, cp.company_name 								as company_name
	, py.payment_method_group						as payment_method_group
	-- , datediff(day, its.order_date, its.paid_date) 	as processing_days
	, count(distinct(order_number))					as sample_total_qty
	from star_schema.fact_item_sold its
		left join star_schema.dim_company cp 		on its.fk_company = cp.id_company
		left join star_schema.dim_store st 			on st.fk_company = cp.id_company
		left join star_schema.dim_payment_method py on its.fk_payment_method = py.id_payment_method
		left join star_schema.dim_date dt 			on its.fk_order_date = dt.id_date
	where 1=1
		and st.is_marketplace_out = 0
		and dt.date_ between (sysdate - 64) and (sysdate - 8)
		and paid_date is not null
		and py.payment_method_group = 'CREDIT CARD'
		and cp.company_code in ('DBR','TRI','KAN')
		and datediff(day, its.order_date, its.paid_date) between 0 and 2 
	group by
	  dt.day_of_week 
	, cp.company_name 							
	, py.payment_method_group 					
	-- , datediff(day, its.order_date, its.paid_date)
	) aux2   on aux1.order_weekday = aux2.order_weekday
				and aux1.company_name = aux2.company_name
				and aux1.payment_method_group = aux2.payment_method_group
),


online_debit_inflow as (
select 
	aux1.company_name, aux1.payment_method_group, aux1.order_weekday, aux1.customer_payment_leadtime_days, 
	aux1.sample_qty, aux2.sample_total_qty, (aux1.sample_qty/aux2.sample_total_qty::float) expected_payment_share
from
	(
	select 
	  dt.day_of_week  								as order_weekday
	, cp.company_name 								as company_name
	, py.payment_method_group						as payment_method_group
	, datediff(day, its.order_date, its.paid_date) 	as customer_payment_leadtime_days
	, count(distinct(order_number)) 				as sample_qty
	from star_schema.fact_item_sold its
		left join star_schema.dim_company cp 		on its.fk_company = cp.id_company
		left join star_schema.dim_store st 			on st.fk_company = cp.id_company
		left join star_schema.dim_payment_method py on its.fk_payment_method = py.id_payment_method
		left join star_schema.dim_date dt 			on its.fk_order_date = dt.id_date
	where 1=1
		and st.is_marketplace_out = 0
		and dt.date_ between (sysdate - 64) and (sysdate - 8)
		and paid_date is not null
		and py.payment_method_group = ('ONLINE DEBIT') -- there's no 'online transfer' to 2016
		and cp.company_code in ('DBR','TRI','KAN')
		and datediff(day, its.order_date, its.paid_date) between 0 and 1 -- max relevant leadtime (>=0.9%)
	group by
	  dt.day_of_week 
	, cp.company_name 							
	, py.payment_method_group 					
	, datediff(day, its.order_date, its.paid_date)
	) aux1
join 
	(
	select 
	  dt.day_of_week  								as order_weekday
	, cp.company_name 								as company_name
	, py.payment_method_group						as payment_method_group
	-- , datediff(day, its.order_date, its.paid_date) 	as processing_days
	, count(distinct(order_number))					as sample_total_qty
	from star_schema.fact_item_sold its
		left join star_schema.dim_company cp 		on its.fk_company = cp.id_company
		left join star_schema.dim_store st 			on st.fk_company = cp.id_company
		left join star_schema.dim_payment_method py on its.fk_payment_method = py.id_payment_method
		left join star_schema.dim_date dt 			on its.fk_order_date = dt.id_date
	where 1=1
		and st.is_marketplace_out = 0
		and dt.date_ between (sysdate - 64) and (sysdate - 8)
		and paid_date is not null
		and py.payment_method_group = ('ONLINE DEBIT')
		and cp.company_code in ('DBR','TRI','KAN')
		and datediff(day, its.order_date, its.paid_date) between 0 and 1 
	group by
	  dt.day_of_week 
	, cp.company_name 							
	, py.payment_method_group 					
	-- , datediff(day, its.order_date, its.paid_date)
	) aux2   on aux1.order_weekday = aux2.order_weekday
				and aux1.company_name = aux2.company_name
				and aux1.payment_method_group = aux2.payment_method_group
),


wallet_payment_inflow as (
select 
	aux1.company_name, aux1.payment_method_group, aux1.order_weekday, aux1.customer_payment_leadtime_days, 
	aux1.sample_qty, aux2.sample_total_qty, (aux1.sample_qty/aux2.sample_total_qty::float) expected_payment_share
from
	(
	select 
	  dt.day_of_week  								as order_weekday
	, cp.company_name 								as company_name
	, py.payment_method_group						as payment_method_group
	, datediff(day, its.order_date, its.paid_date) 	as customer_payment_leadtime_days
	, count(distinct(order_number)) 				as sample_qty
	from star_schema.fact_item_sold its
		left join star_schema.dim_company cp 		on its.fk_company = cp.id_company
		left join star_schema.dim_store st 			on st.fk_company = cp.id_company
		left join star_schema.dim_payment_method py on its.fk_payment_method = py.id_payment_method
		left join star_schema.dim_date dt 			on its.fk_order_date = dt.id_date
	where 1=1
		and st.is_marketplace_out = 0
		and dt.date_ between (sysdate - 64) and (sysdate - 8)
		and paid_date is not null
		and py.payment_method_group = 'WALLET PAYMENT' -- only TRICAE and KAN, for now
		and cp.company_code in ('DBR','TRI','KAN')
		and datediff(day, its.order_date, its.paid_date) between 0 and 6 -- max relevant leadtime (>=0.9%)
	group by
	  dt.day_of_week 
	, cp.company_name 							
	, py.payment_method_group 					
	, datediff(day, its.order_date, its.paid_date)
	) aux1
join 
	(
	select 
	  dt.day_of_week  								as order_weekday
	, cp.company_name 								as company_name
	, py.payment_method_group						as payment_method_group
	-- , datediff(day, its.order_date, its.paid_date) 	as processing_days
	, count(distinct(order_number))					as sample_total_qty
	from star_schema.fact_item_sold its
		left join star_schema.dim_company cp 		on its.fk_company = cp.id_company
		left join star_schema.dim_store st 			on st.fk_company = cp.id_company
		left join star_schema.dim_payment_method py on its.fk_payment_method = py.id_payment_method
		left join star_schema.dim_date dt 			on its.fk_order_date = dt.id_date
	where 1=1
		and st.is_marketplace_out = 0
		and dt.date_ between (sysdate - 64) and (sysdate - 8)
		and paid_date is not null
		and py.payment_method_group = 'WALLET PAYMENT'
		and cp.company_code in ('DBR','TRI','KAN')
		and datediff(day, its.order_date, its.paid_date) between 0 and 6 
	group by
	  dt.day_of_week 
	, cp.company_name 							
	, py.payment_method_group 					
	-- , datediff(day, its.order_date, its.paid_date)
	) aux2   on aux1.order_weekday = aux2.order_weekday
				and aux1.company_name = aux2.company_name
				and aux1.payment_method_group = aux2.payment_method_group
)




------------
-- select
--   aux.order_date
-- , aux.store_name
-- , aux.payment_method_group
-- , aux.total_gmv_bef_cnc_credit_card as total_gmv_bef_cnc
-- , aux.total_gmv_less_ref_bef_cnc_credit_card as total_gmv_less_ref_bef_cnc
-- , aux.total_gmv_less_ref_aft_cnc_credit_card as total_gmv_less_ref_aft_cnc
-- from (

(select aux.*, (aux.customer_paid_date_est + aux.money_inflow_leadtime) as inflow_date_est
from (select 
  dt.date_ as order_date
	, (dt.date_ + cf.customer_payment_leadtime_days) 	as customer_paid_date_est
	, case 
		when ma.store_name = 'DAFITI BRAZIL' then
			case
				when to_char((dt.date_ + cf.customer_payment_leadtime_days), 'd') in ('1','7') then 2
				when to_char((dt.date_ + cf.customer_payment_leadtime_days), 'd') in ('2','3','4','5') then 1
				when to_char((dt.date_ + cf.customer_payment_leadtime_days), 'd') = '6' then 3
				else 0
			end
		else 
			(case
				when to_char((dt.date_ + cf.customer_payment_leadtime_days), 'd') = '1' then 1
				when to_char((dt.date_ + cf.customer_payment_leadtime_days), 'd') = '7' then 2
				else 0
			end)
	 end as money_inflow_leadtime
	, dt.day_of_week  									as order_weekday

, ma.store_name as store_name 
, 'BANK SLIP' 	as payment_method_group
-- , round(coalesce(ma.total_gmv_bef_cnc_bef_ret*ds.gmv_daily_share,0),0) as total_gmv_bef_cnc
-- , round(coalesce(ma.total_gmv_bef_cnc_bef_ret*ds.gmv_daily_share*pm.credit_card_share*(cf.expected_payment_share),0),0) as total_gmv_bef_cnc_credit_card
, round(coalesce(ma.total_gmv_bef_cnc_bef_ret*ds.gmv_daily_share
	*pm.bank_slip_share*(cf.expected_payment_share),0),0) as total_gmv_bef_cnc
-- , round(coalesce(ma.total_gmv_bef_cnc_bef_ret*ds.gmv_daily_share*pm.other_share,0),0) as total_gmv_bef_cnc_other
-- , round(coalesce(ma.total_gmv_bef_cnc_bef_ret*ds.gmv_daily_share*(1 - rc.refund_voucher_rate),0),0) as total_gmv_less_ref_bef_cnc
-- , round(coalesce(ma.total_gmv_bef_cnc_bef_ret*ds.gmv_daily_share*pm.credit_card_share*(1 - rc.refund_voucher_rate)*(cf.expected_payment_share),0),0) as total_gmv_less_ref_bef_cnc_credit_card
, round(coalesce(ma.total_gmv_bef_cnc_bef_ret*ds.gmv_daily_share
	*pm.bank_slip_share*(1 - rc.refund_voucher_rate)*(cf.expected_payment_share),0),0) as total_gmv_less_ref_bef_cnc
-- , round(coalesce(ma.total_gmv_bef_cnc_bef_ret*ds.gmv_daily_share*pm.other_share*(1 - rc.refund_voucher_rate),0),0) as total_gmv_less_ref_bef_cnc_other
-- , round(coalesce(ma.total_gmv_bef_cnc_bef_ret*ds.gmv_daily_share*(1 - rc.refund_voucher_rate)*(1- cn.cancellation_rate),0),0) as total_gmv_less_ref_aft_cnc
-- , round(coalesce(ma.total_gmv_bef_cnc_bef_ret*ds.gmv_daily_share*pm.credit_card_share*(1 - rc.refund_voucher_rate)*(1- cn.cancellation_rate_credit_card)*(cf.expected_payment_share),0),0) as total_gmv_less_ref_aft_cnc_credit_card
, round(coalesce(ma.total_gmv_bef_cnc_bef_ret*ds.gmv_daily_share*pm.bank_slip_share
	*(1 - rc.refund_voucher_rate)*(1- cn.cancellation_rate_bank_slip)*(cf.expected_payment_share),0),0) as total_gmv_less_ref_aft_cnc
-- , round(coalesce(ma.total_gmv_bef_cnc_bef_ret*ds.gmv_daily_share*pm.other_share*(1 - rc.refund_voucher_rate)*(1- cn.cancellation_rate_other),0),0) as total_gmv_less_ref_aft_cnc_other
from targets.tgt_mkt_monthly_absolute ma
	join targets.tgt_mkt_daily_share ds  on ma.country_name = ds.country_name
	         and ma.store_name = ds.store_name
	         and ma.year = ds.year
	         and ma.month_of_year = ds.month_of_year
	join star_schema.dim_currency curr 	on curr.currency_code = ma.currency_code
	join star_schema.dim_date dt 		on cast(concat(concat(ma.year, ma.month_of_year), ds.day_of_month) as bigint) = dt.id_date
	join pay_meth pm on ma.store_name = pm.company_name
	join ref rc on ma.store_name = rc.company_name
	join cnc cn on ma.store_name = cn.company_name
	join bank_slip_inflow cf 			on ma.store_name = cf.company_name
											and dt.day_of_week = cf.order_weekday

where 1=1
	and ma.country_name = 'BRAZIL'
	and ma.store_name in ('DAFITI BRAZIL','TRICAE','KANUI')) aux
where 
	datediff(day, aux.order_date, coalesce(aux.customer_paid_date_est, aux.order_date)) >= 0 -- detected erros in DB, where order_date > paid_date. insignificant # occurrences.
	)



UNION ALL



(select aux.*, (aux.customer_paid_date_est + aux.money_inflow_leadtime) as inflow_date_est
from (select 
  dt.date_ as order_date
	, (dt.date_ + cf.customer_payment_leadtime_days) 	as customer_paid_date_est
	, case 
		when to_char((dt.date_ + cf.customer_payment_leadtime_days), 'd') in ('1','2','3','4','5') then 1
		when to_char((dt.date_ + cf.customer_payment_leadtime_days), 'd') = '6' then 3
		when to_char((dt.date_ + cf.customer_payment_leadtime_days), 'd') = '7' then 2
		else 0
	 end 											 	as money_inflow_leadtime
	, dt.day_of_week  									as order_weekday
	, ma.store_name as store_name
	, 'CREDIT CARD' as payment_method_group
	-- , round(coalesce(ma.total_gmv_bef_cnc_bef_ret*ds.gmv_daily_share,0),0) as total_gmv_bef_cnc
	, round(coalesce(ma.total_gmv_bef_cnc_bef_ret*ds.gmv_daily_share*pm.credit_card_share*(cf.expected_payment_share),0),0) as total_gmv_bef_cnc
	-- , round(coalesce(ma.total_gmv_bef_cnc_bef_ret*ds.gmv_daily_share*pm.bank_slip_share,0),0) as total_gmv_bef_cnc_bank_slip
	-- , round(coalesce(ma.total_gmv_bef_cnc_bef_ret*ds.gmv_daily_share*pm.other_share,0),0) as total_gmv_bef_cnc_other
	-- , round(coalesce(ma.total_gmv_bef_cnc_bef_ret*ds.gmv_daily_share*(1 - rc.refund_voucher_rate),0),0) as total_gmv_less_ref_bef_cnc
	, round(coalesce(ma.total_gmv_bef_cnc_bef_ret*ds.gmv_daily_share*pm.credit_card_share*(1 - rc.refund_voucher_rate)
			*(cf.expected_payment_share),0),0) as total_gmv_less_ref_bef_cnc
	-- , round(coalesce(ma.total_gmv_bef_cnc_bef_ret*ds.gmv_daily_share*pm.bank_slip_share*(1 - rc.refund_voucher_rate),0),0) as total_gmv_less_ref_bef_cnc_bank_slip
	-- , round(coalesce(ma.total_gmv_bef_cnc_bef_ret*ds.gmv_daily_share*pm.other_share*(1 - rc.refund_voucher_rate),0),0) as total_gmv_less_ref_bef_cnc_other
	-- , round(coalesce(ma.total_gmv_bef_cnc_bef_ret*ds.gmv_daily_share*(1 - rc.refund_voucher_rate)*(1- cn.cancellation_rate),0),0) as total_gmv_less_ref_aft_cnc
	, round(coalesce(ma.total_gmv_bef_cnc_bef_ret*ds.gmv_daily_share*pm.credit_card_share*(1 - rc.refund_voucher_rate)
			*(1- cn.cancellation_rate_credit_card)*(cf.expected_payment_share),0),0) as total_gmv_less_ref_aft_cnc
	-- , round(coalesce(ma.total_gmv_bef_cnc_bef_ret*ds.gmv_daily_share*pm.bank_slip_share*(1 - rc.refund_voucher_rate)*(1- cn.cancellation_rate_bank_slip),0),0) as total_gmv_less_ref_aft_cnc_bank_slip
	-- , round(coalesce(ma.total_gmv_bef_cnc_bef_ret*ds.gmv_daily_share*pm.other_share*(1 - rc.refund_voucher_rate)*(1- cn.cancellation_rate_other),0),0) as total_gmv_less_ref_aft_cnc_other
from targets.tgt_mkt_monthly_absolute ma
	join targets.tgt_mkt_daily_share ds on ma.country_name = ds.country_name
	         and ma.store_name = ds.store_name
	         and ma.year = ds.year
	         and ma.month_of_year = ds.month_of_year
	join star_schema.dim_currency curr 	on curr.currency_code = ma.currency_code
	join star_schema.dim_date dt 		on cast(concat(concat(ma.year, ma.month_of_year), ds.day_of_month) as bigint) = dt.id_date
	join pay_meth pm 					on ma.store_name = pm.company_name
	join ref rc 						on ma.store_name = rc.company_name
	join cnc cn 						on ma.store_name = cn.company_name
	join credit_card_inflow cf 			on ma.store_name = cf.company_name
											and dt.day_of_week = cf.order_weekday

where 1=1
	and ma.country_name = 'BRAZIL'
	and ma.store_name in ('DAFITI BRAZIL','TRICAE','KANUI')) aux
where 
	datediff(day, aux.order_date, coalesce(aux.customer_paid_date_est, aux.order_date)) >= 0 -- detected erros in DB, where order_date > paid_date. insignificant # occurrences.
	)



UNION ALL



(select aux.*, (aux.customer_paid_date_est + aux.money_inflow_leadtime) as inflow_date_est
from (select 
  dt.date_ as order_date
	, (dt.date_ + cf.customer_payment_leadtime_days) 	as customer_paid_date_est
	, case -- same rules as Bank Slip
		when to_char((dt.date_ + cf.customer_payment_leadtime_days), 'd') in ('1','7') then 2
		when to_char((dt.date_ + cf.customer_payment_leadtime_days), 'd') in ('2','3','4','5') then 1
		when to_char((dt.date_ + cf.customer_payment_leadtime_days), 'd') = '6' then 3
		else 0
	end 											 	as money_inflow_leadtime
	, dt.day_of_week  									as order_weekday
	, ma.store_name as store_name
	, 'ONLINE DEBIT' as payment_method_group
	-- , round(coalesce(ma.total_gmv_bef_cnc_bef_ret*ds.gmv_daily_share,0),0) as total_gmv_bef_cnc
	, round(coalesce(ma.total_gmv_bef_cnc_bef_ret*ds.gmv_daily_share*pm.online_debit_share*(cf.expected_payment_share),0),0) as total_gmv_bef_cnc
	-- , round(coalesce(ma.total_gmv_bef_cnc_bef_ret*ds.gmv_daily_share*pm.bank_slip_share,0),0) as total_gmv_bef_cnc_bank_slip
	-- , round(coalesce(ma.total_gmv_bef_cnc_bef_ret*ds.gmv_daily_share*pm.other_share,0),0) as total_gmv_bef_cnc_other
	-- , round(coalesce(ma.total_gmv_bef_cnc_bef_ret*ds.gmv_daily_share*(1 - rc.refund_voucher_rate),0),0) as total_gmv_less_ref_bef_cnc
	, round(coalesce(ma.total_gmv_bef_cnc_bef_ret*ds.gmv_daily_share*pm.online_debit_share*(1 - rc.refund_voucher_rate)
			*(cf.expected_payment_share),0),0) as total_gmv_less_ref_bef_cnc
	-- , round(coalesce(ma.total_gmv_bef_cnc_bef_ret*ds.gmv_daily_share*pm.bank_slip_share*(1 - rc.refund_voucher_rate),0),0) as total_gmv_less_ref_bef_cnc_bank_slip
	-- , round(coalesce(ma.total_gmv_bef_cnc_bef_ret*ds.gmv_daily_share*pm.other_share*(1 - rc.refund_voucher_rate),0),0) as total_gmv_less_ref_bef_cnc_other
	-- , round(coalesce(ma.total_gmv_bef_cnc_bef_ret*ds.gmv_daily_share*(1 - rc.refund_voucher_rate)*(1- cn.cancellation_rate),0),0) as total_gmv_less_ref_aft_cnc
	, round(coalesce(ma.total_gmv_bef_cnc_bef_ret*ds.gmv_daily_share*pm.online_debit_share*(1 - rc.refund_voucher_rate)
			*(1- cn.cancellation_rate_credit_card)*(cf.expected_payment_share),0),0) as total_gmv_less_ref_aft_cnc
	-- , round(coalesce(ma.total_gmv_bef_cnc_bef_ret*ds.gmv_daily_share*pm.bank_slip_share*(1 - rc.refund_voucher_rate)*(1- cn.cancellation_rate_bank_slip),0),0) as total_gmv_less_ref_aft_cnc_bank_slip
	-- , round(coalesce(ma.total_gmv_bef_cnc_bef_ret*ds.gmv_daily_share*pm.other_share*(1 - rc.refund_voucher_rate)*(1- cn.cancellation_rate_other),0),0) as total_gmv_less_ref_aft_cnc_other
from targets.tgt_mkt_monthly_absolute ma
	join targets.tgt_mkt_daily_share ds on ma.country_name = ds.country_name
	         and ma.store_name = ds.store_name
	         and ma.year = ds.year
	         and ma.month_of_year = ds.month_of_year
	join star_schema.dim_currency curr 	on curr.currency_code = ma.currency_code
	join star_schema.dim_date dt 		on cast(concat(concat(ma.year, ma.month_of_year), ds.day_of_month) as bigint) = dt.id_date
	join pay_meth pm 					on ma.store_name = pm.company_name
	join ref rc 						on ma.store_name = rc.company_name
	join cnc cn 						on ma.store_name = cn.company_name
	join credit_card_inflow cf 			on ma.store_name = cf.company_name
											and dt.day_of_week = cf.order_weekday

where 1=1
	and ma.country_name = 'BRAZIL'
	and ma.store_name in ('DAFITI BRAZIL','TRICAE','KANUI')) aux
where 
	datediff(day, aux.order_date, coalesce(aux.customer_paid_date_est, aux.order_date)) >= 0 -- detected erros in DB, where order_date > paid_date. insignificant # occurrences.
	)


UNION ALL


(select aux.*, (aux.customer_paid_date_est + aux.money_inflow_leadtime) as inflow_date_est
from (select 
  dt.date_ as order_date
	, (dt.date_ + cf.customer_payment_leadtime_days) 	as customer_paid_date_est
	, case 
		when to_char((dt.date_ + cf.customer_payment_leadtime_days), 'd') in ('1','2','3') then 3
		when to_char((dt.date_ + cf.customer_payment_leadtime_days), 'd') = '7' then 4
		when to_char((dt.date_ + cf.customer_payment_leadtime_days), 'd') in ('4','5','6') then 5
		else 0
	 end 											 	as money_inflow_leadtime
	, dt.day_of_week  									as order_weekday
	, ma.store_name as store_name
	, 'WALLET PAYMENT' as payment_method_group
	-- , round(coalesce(ma.total_gmv_bef_cnc_bef_ret*ds.gmv_daily_share,0),0) as total_gmv_bef_cnc
	, round(coalesce(ma.total_gmv_bef_cnc_bef_ret*ds.gmv_daily_share*pm.wallet_share*(cf.expected_payment_share),0),0) as total_gmv_bef_cnc
	-- , round(coalesce(ma.total_gmv_bef_cnc_bef_ret*ds.gmv_daily_share*pm.bank_slip_share,0),0) as total_gmv_bef_cnc_bank_slip
	-- , round(coalesce(ma.total_gmv_bef_cnc_bef_ret*ds.gmv_daily_share*pm.other_share,0),0) as total_gmv_bef_cnc_other
	-- , round(coalesce(ma.total_gmv_bef_cnc_bef_ret*ds.gmv_daily_share*(1 - rc.refund_voucher_rate),0),0) as total_gmv_less_ref_bef_cnc
	, round(coalesce(ma.total_gmv_bef_cnc_bef_ret*ds.gmv_daily_share*pm.wallet_share*(1 - rc.refund_voucher_rate)
			*(cf.expected_payment_share),0),0) as total_gmv_less_ref_bef_cnc
	-- , round(coalesce(ma.total_gmv_bef_cnc_bef_ret*ds.gmv_daily_share*pm.bank_slip_share*(1 - rc.refund_voucher_rate),0),0) as total_gmv_less_ref_bef_cnc_bank_slip
	-- , round(coalesce(ma.total_gmv_bef_cnc_bef_ret*ds.gmv_daily_share*pm.other_share*(1 - rc.refund_voucher_rate),0),0) as total_gmv_less_ref_bef_cnc_other
	-- , round(coalesce(ma.total_gmv_bef_cnc_bef_ret*ds.gmv_daily_share*(1 - rc.refund_voucher_rate)*(1- cn.cancellation_rate),0),0) as total_gmv_less_ref_aft_cnc
	, round(coalesce(ma.total_gmv_bef_cnc_bef_ret*ds.gmv_daily_share*pm.wallet_share*(1 - rc.refund_voucher_rate)
			*(1- cn.cancellation_rate_credit_card)*(cf.expected_payment_share),0),0) as total_gmv_less_ref_aft_cnc
	-- , round(coalesce(ma.total_gmv_bef_cnc_bef_ret*ds.gmv_daily_share*pm.bank_slip_share*(1 - rc.refund_voucher_rate)*(1- cn.cancellation_rate_bank_slip),0),0) as total_gmv_less_ref_aft_cnc_bank_slip
	-- , round(coalesce(ma.total_gmv_bef_cnc_bef_ret*ds.gmv_daily_share*pm.other_share*(1 - rc.refund_voucher_rate)*(1- cn.cancellation_rate_other),0),0) as total_gmv_less_ref_aft_cnc_other
from targets.tgt_mkt_monthly_absolute ma
	join targets.tgt_mkt_daily_share ds on ma.country_name = ds.country_name
	         and ma.store_name = ds.store_name
	         and ma.year = ds.year
	         and ma.month_of_year = ds.month_of_year
	join star_schema.dim_currency curr 	on curr.currency_code = ma.currency_code
	join star_schema.dim_date dt 		on cast(concat(concat(ma.year, ma.month_of_year), ds.day_of_month) as bigint) = dt.id_date
	join pay_meth pm 					on ma.store_name = pm.company_name
	join ref rc 						on ma.store_name = rc.company_name
	join cnc cn 						on ma.store_name = cn.company_name
	join credit_card_inflow cf 			on ma.store_name = cf.company_name
											and dt.day_of_week = cf.order_weekday

where 1=1
	and ma.country_name = 'BRAZIL'
	and ma.store_name in ('DAFITI BRAZIL','TRICAE','KANUI')) aux
where 
	datediff(day, aux.order_date, coalesce(aux.customer_paid_date_est, aux.order_date)) >= 0 -- detected erros in DB, where order_date > paid_date. insignificant # occurrences.
	)


UNION ALL


(select aux.*, (customer_paid_date_est + money_inflow_leadtime) as inflow_date_est
from (select 
  dt.date_ as order_date
	, (dt.date_ + 0) as customer_paid_date_est
	, 0 money_inflow_leadtime
	, dt.day_of_week as order_weekday
, ma.store_name 	 as store_name
, 'OTHER' 			 as payment_method_group
-- , round(coalesce(ma.total_gmv_bef_cnc_bef_ret*ds.gmv_daily_share,0),0) as total_gmv_bef_cnc
-- , round(coalesce(ma.total_gmv_bef_cnc_bef_ret*ds.gmv_daily_share*pm.credit_card_share*(cf.expected_payment_share),0),0) as total_gmv_bef_cnc_credit_card
-- , round(coalesce(ma.total_gmv_bef_cnc_bef_ret*ds.gmv_daily_share*pm.bank_slip_share,0),0) as total_gmv_bef_cnc_bank_slip
, round(coalesce(ma.total_gmv_bef_cnc_bef_ret*ds.gmv_daily_share*pm.other_share,0),0) as total_gmv_bef_cnc_other
-- , round(coalesce(ma.total_gmv_bef_cnc_bef_ret*ds.gmv_daily_share*(1 - rc.refund_voucher_rate),0),0) as total_gmv_less_ref_bef_cnc
-- , round(coalesce(ma.total_gmv_bef_cnc_bef_ret*ds.gmv_daily_share*pm.credit_card_share*(1 - rc.refund_voucher_rate)*(cf.expected_payment_share),0),0) as total_gmv_less_ref_bef_cnc_credit_card
-- , round(coalesce(ma.total_gmv_bef_cnc_bef_ret*ds.gmv_daily_share*pm.bank_slip_share*(1 - rc.refund_voucher_rate),0),0) as total_gmv_less_ref_bef_cnc_bank_slip
, round(coalesce(ma.total_gmv_bef_cnc_bef_ret*ds.gmv_daily_share*pm.other_share*(1 - rc.refund_voucher_rate),0),0) as total_gmv_less_ref_bef_cnc_other
-- , round(coalesce(ma.total_gmv_bef_cnc_bef_ret*ds.gmv_daily_share*(1 - rc.refund_voucher_rate)*(1- cn.cancellation_rate),0),0) as total_gmv_less_ref_aft_cnc
-- , round(coalesce(ma.total_gmv_bef_cnc_bef_ret*ds.gmv_daily_share*pm.credit_card_share*(1 - rc.refund_voucher_rate)*(1- cn.cancellation_rate_credit_card)*(cf.expected_payment_share),0),0) as total_gmv_less_ref_aft_cnc_credit_card
-- , round(coalesce(ma.total_gmv_bef_cnc_bef_ret*ds.gmv_daily_share*pm.bank_slip_share*(1 - rc.refund_voucher_rate)*(1- cn.cancellation_rate_bank_slip),0),0) as total_gmv_less_ref_aft_cnc_bank_slip
, round(coalesce(ma.total_gmv_bef_cnc_bef_ret*ds.gmv_daily_share*pm.other_share
	*(1 - rc.refund_voucher_rate)*(1- cn.cancellation_rate_other),0),0) as total_gmv_less_ref_aft_cnc_other
from targets.tgt_mkt_monthly_absolute ma
	join targets.tgt_mkt_daily_share ds  on ma.country_name = ds.country_name
	         and ma.store_name = ds.store_name
	         and ma.year = ds.year
	         and ma.month_of_year = ds.month_of_year
	join star_schema.dim_currency curr on curr.currency_code = ma.currency_code
	join star_schema.dim_date dt on cast(concat(concat(ma.year, ma.month_of_year), ds.day_of_month) as bigint) = dt.id_date
	join pay_meth pm on ma.store_name = pm.company_name
	join ref rc on ma.store_name = rc.company_name
	join cnc cn on ma.store_name = cn.company_name

where 1=1
	and ma.country_name = 'BRAZIL'
	and ma.store_name in ('DAFITI BRAZIL','TRICAE','KANUI')) aux
where 
	datediff(day, aux.order_date, coalesce(aux.customer_paid_date_est, aux.order_date)) >= 0 -- detected erros in DB, where order_date > paid_date. insignificant # occurrences.
	)

