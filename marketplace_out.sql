with tricae as (
select 
	aux1.company_name, aux1.store_name, aux1.customer_payment_leadtime_days, 
	aux1.sample_qty, aux2.sample_total_qty, (aux1.sample_qty/aux2.sample_total_qty::float) expected_payment_share
from
	(
	select 
	  --dt.day_of_week  								as order_weekday
	 cp.company_name 								as company_name
	, st.store_name 								as store_name
	, datediff(day, its.order_date, its.paid_date) 	as customer_payment_leadtime_days
	, count(distinct(its.src_bob_fk_sales_order_item)) 				as sample_qty
	from star_schema.fact_item_sold its
		--join raw_bob_tricae.sales_order_item soi on its.src_bob_fk_sales_order_item = soi.id_sales_order_item 
		left join star_schema.dim_company cp 		on its.fk_company = cp.id_company
		left join star_schema.dim_store st 			on st.id_store = its.fk_store
		left join star_schema.dim_date dt 			on its.fk_order_date = dt.id_date
	where 1=1
		and st.is_marketplace_out = 1
		and dt.date_ between (sysdate - 64) and (sysdate - 7) --
		and paid_date is not null
		and cp.company_code = 'TRI'
		and datediff(day, its.order_date, its.paid_date) between 0 and 6 -- max relevant leadtime (>=0.9)
	group by
	 -- dt.day_of_week 
	 cp.company_name 							
	, st.store_name 					
	, datediff(day, its.order_date, its.paid_date)
	) aux1
join 
	(
	select 
	  --dt.day_of_week  								as order_weekday
	 cp.company_name 								as company_name
	, st.store_name 								as store_name
	--, datediff(day, its.order_date, its.paid_date) 	as customer_payment_leadtime_days
	, count(distinct(its.src_bob_fk_sales_order_item)) 				as sample_total_qty
	from star_schema.fact_item_sold its
		--join raw_bob_tricae.sales_order_item soi on its.src_bob_fk_sales_order_item = soi.id_sales_order_item 
		left join star_schema.dim_company cp 		on its.fk_company = cp.id_company
		left join star_schema.dim_store st 			on st.id_store = its.fk_store
		left join star_schema.dim_date dt 			on its.fk_order_date = dt.id_date
	where 1=1
		and st.is_marketplace_out = 1
		and dt.date_ between (sysdate - 64) and (sysdate - 7) --
		and paid_date is not null
		and cp.company_code = 'TRI'
		and datediff(day, its.order_date, its.paid_date) between 0 and 6 -- max relevant leadtime (>=0.9)
	group by
	 -- dt.day_of_week 
	 cp.company_name 							
	, st.store_name 					
	--, datediff(day, its.order_date, its.paid_date)
	) aux2 on --aux1.order_weekday = aux2.order_weekday and 
			aux1.company_name = aux2.company_name and aux1.store_name = aux2.store_name
),


item_exp_tricae as (
	select 	  
		distinct
		soi.id_sales_order_item										as id_sales_order_item
		, cp.id_company 											as id_company
		, st.id_store												as id_store
		, case 
			when dt.date_ < (sysdate - 7) then date(its.paid_date) 
			else (dt.date_ + cf.customer_payment_leadtime_days) 		
		 end 														as customer_paid_date_est
		, case 
			when dt.date_ < (sysdate - 7) then date(its.paid_date + soi.shipping_time_in_workdays) 
			else (dt.date_ + cf.customer_payment_leadtime_days + soi.shipping_time_in_workdays) 
		 end	 													as expedition_date_est
	from star_schema.fact_item_sold its
		left join star_schema.dim_date dt 		on its.fk_order_date = dt.id_date
		left join star_schema.dim_company cp 	on its.fk_company = cp.id_company 
		left join star_schema.dim_store st 		on st.id_store = its.fk_store
		left join tricae cf 	 				on cp.company_name = cf.company_name
													and st.store_name = cf.store_name
													--and dt.day_of_week = cf.order_weekday
		left join (select
			 distinct 
			 soi.id_sales_order_item									as id_sales_order_item
			, case when fk_catalog_shipment_type = 3 then 1 else 0 end 	as is_crossdocking
			, (case when fk_catalog_shipment_type = 3 then shipment_supplier_delivery_time else 0 end) 
				+ (soi.shipment_warehouse_processing_time) 				as shipping_time_in_workdays
			from raw_bob_tricae.sales_order_item soi
			where 1=1
				and soi.created_at between (sysdate - 95) and (sysdate - 1) 
			) soi on its.src_bob_fk_sales_order_item = soi.id_sales_order_item  
	 where 1
		and st.is_marketplace_out = 1 -- no marketplace out
		and its.is_invalid = 0
		and cp.company_code = 'TRI'
		and dt.date_ between (sysdate - 95) and (sysdate - 1) 
	 ),

rule_expedition_date_walmart as (
select 
	aux1.expedition_date 	as expedition_date
	,aux2.inflow_date			as inflow_date

from (select 
	date(dt.date_) as expedition_date
	,case 
		when (extract(d from dt.date_) between 26 and 31) 
			then extract(y from dateadd(month,2,dt.date_))::VARCHAR(20) 
				|| extract(mon from dateadd(month,2,dt.date_))::VARCHAR(20)
		else
			extract(y from dateadd(month,1,dt.date_))::VARCHAR(20) 
				||  extract(mon from dateadd(month,1,dt.date_))::VARCHAR(20)
	end as year_month

	from star_schema.dim_date dt

	where dt.iso_date between (sysdate - 95) and (sysdate + 60)
) aux1

left join 

(select 
	dt.date_ as inflow_date
	,extract(y from dt.date_)::VARCHAR(20) ||  extract(mon from dt.date_)::VARCHAR(20) as year_month
from star_schema.dim_date dt
where 1
	and dt.day_of_week = '4' 
	and (dt.day_of_month::int) between 8 and 14
	and dt.iso_date between (sysdate - 95) and (sysdate + 150) 

) aux2

on aux2.year_month = aux1.year_month)


--  
	-- Tricae
select 
	aux.*
	,case
		when aux.store_name = 'B2W' then total_gmv_less_refund_voucher_aft_cnc_est*(1-0.13)
		when aux.store_name = 'EXTRA' then total_gmv_less_refund_voucher_aft_cnc_est*(1-0.125)
		when aux.store_name = 'EXTERNALSHOP' then total_gmv_less_refund_voucher_aft_cnc_est*(1-0.125)
	end as total_gmv_less_refund_voucher_commission_aft_cnc_est
from (select 
	dt.date_ 						as order_date
	, itexp.customer_paid_date_est	as customer_paid_date_est
	, itexp.expedition_date_est		as expedition_date_est
	, case 
		when st.store_name = 'B2W' then 
			case
				when (extract(d from itexp.expedition_date_est) between 1 and 15) then to_char(dateadd(month,1,itexp.expedition_date_est), 'YYYY-MM-01')::date
				else to_char(dateadd(month,1,itexp.expedition_date_est), 'YYYY-MM-15')::date
				
			end
		when st.store_name = 'EXTRA' then 
			case 
				when (extract(d from itexp.expedition_date_est) between 5 and 15) then to_char(itexp.expedition_date_est, 'YYYY-MM-25')::date
				when (extract(d from itexp.expedition_date_est) between 16 and 25) then to_char(dateadd(month,1,itexp.expedition_date_est), 'YYYY-MM-05')::date
				else 
					case 
						when extract(d from itexp.expedition_date_est) between 26 and 31 then to_char(dateadd(month,1,itexp.expedition_date_est), 'YYYY-MM-15')::date
						when extract(d from itexp.expedition_date_est) between 1 and 4 then to_char(itexp.expedition_date_est, 'YYYY-MM-15')::date
					end 
			end
		when st.store_name = 'EXTERNALSHOP' then to_char(dtwal.inflow_date, 'YYYY-MM-DD')::date

	 end 										as inflow_date_est
	--, dt.day_of_week  							as order_weekday
	, cp.company_name 							as company_name
	, st.store_name								as store_name
	
	-- E SE cf.expected_payment_share eh NULL coalesce(cf.expected_payment_share, 1)
	, sum(coalesce(its.total_gmv, 0)*(cf.expected_payment_share)) 											as total_gmv_bef_cnc
	, sum((coalesce(its.total_gmv, 0) 
		- coalesce(its.refund_product_value, 0) 
		- coalesce(its.refund_shipping_value, 0)
		- coalesce(its.refund_gift_wrap_value, 0))*(cf.expected_payment_share))								as total_gmv_less_refund_voucher_bef_cnc
	, sum(case 
		when dt.date_ < (sysdate - 7) then	-- actual
			case
				when its.is_paid = 1 then (coalesce(its.total_gmv, 0) - coalesce(its.refund_product_value, 0) 
							- coalesce(its.refund_shipping_value, 0) - coalesce(its.refund_gift_wrap_value, 0))
				else 0
			end
		else 								-- forecast
		(1 - its.bef_paid_cancelled_estimated_rate
				 )*(coalesce(its.total_gmv, 0) - coalesce(its.refund_product_value, 0) - coalesce(its.refund_shipping_value, 0) - coalesce(its.refund_gift_wrap_value, 0))
	    end * (cf.expected_payment_share)) 																	as total_gmv_less_refund_voucher_aft_cnc_est

from star_schema.fact_item_sold its
	left join star_schema.dim_country ct 							on its.fk_sale_country = ct.id_country
	left join star_schema.dim_company cp 							on its.fk_company = cp.id_company
	left join star_schema.dim_store st 								on its.fk_store = st.id_store
	left join star_schema.dim_device_platform dp 					on its.fk_device_platform = dp.id_device_platform
	left join star_schema.dim_channel_partner_google_analytics cpga on its.fk_last_click_channel_partner_google_analytics = cpga.id_channel_partner_google_analytics
	left join star_schema.dim_product_config pdc 					on its.fk_product_config = pdc.id_product_config
	left join star_schema.dim_date dt 								on its.fk_order_date = dt.id_date
	left join tricae cf 	 										on cp.company_name = cf.company_name
																			and st.store_name = cf.store_name
																			--and dt.day_of_week = cf.order_weekday
	left join item_exp_tricae itexp 								on itexp.id_sales_order_item = its.src_bob_fk_sales_order_item 	
																		and itexp.id_company = cp.id_company
																		and itexp.id_store = st.id_store		
	left join rule_expedition_date_walmart dtwal 					on dtwal.expedition_date = itexp.expedition_date_est					
																			
where 1=1
	and st.is_marketplace_out = 1 -- no marketplace out
	and its.is_invalid = 0
	and cp.company_code = 'TRI'
	and dt.date_ between (sysdate - 95) and (sysdate - 1) 

group by
  dt.date_ 
, itexp.customer_paid_date_est
, itexp.expedition_date_est
, case 
	when st.store_name = 'B2W' then 
		case
			when (extract(d from itexp.expedition_date_est) between 1 and 15) then to_char(dateadd(month,1,itexp.expedition_date_est), 'YYYY-MM-01')::date
			else to_char(dateadd(month,1,itexp.expedition_date_est), 'YYYY-MM-15')::date
			
		end
	when st.store_name = 'EXTRA' then 
		case 
			when (extract(d from itexp.expedition_date_est) between 5 and 15) then to_char(itexp.expedition_date_est, 'YYYY-MM-25')::date
			when (extract(d from itexp.expedition_date_est) between 16 and 25) then to_char(dateadd(month,1,itexp.expedition_date_est), 'YYYY-MM-05')::date
			else 
				case 
					when extract(d from itexp.expedition_date_est) between 26 and 31 then to_char(dateadd(month,1,itexp.expedition_date_est), 'YYYY-MM-15')::date
					when extract(d from itexp.expedition_date_est) between 1 and 4 then to_char(itexp.expedition_date_est, 'YYYY-MM-15')::date
				end 
		end
	when st.store_name = 'EXTERNALSHOP' then to_char(dtwal.inflow_date, 'YYYY-MM-DD')::date

 end
--, dt.day_of_week
, cp.company_name
, st.store_name) aux

where 
	datediff(day, aux.order_date, coalesce(aux.customer_paid_date_est, aux.order_date)) >= 0 -- detected erros in DB, where order_date > paid_date. insignificant # occurrences.
	
