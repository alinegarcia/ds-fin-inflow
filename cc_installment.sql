select 
	cp.company_name 								as company_name
	,its.number_of_installments
	,case when its.number_of_installments >= 1 then date(its.paid_date) + 1*30   end as date_1_inst
	,case when its.number_of_installments >= 2 then date(its.paid_date) + 2*30   end as date_2_inst
	,case when its.number_of_installments >= 3 then date(its.paid_date) + 3*30   end as date_3_inst
	,case when its.number_of_installments >= 4 then date(its.paid_date) + 4*30   end as date_4_inst
	,case when its.number_of_installments >= 5 then date(its.paid_date) + 5*30   end as date_5_inst
	,case when its.number_of_installments >= 6 then date(its.paid_date) + 6*30   end as date_6_inst
	,case when its.number_of_installments >= 7 then date(its.paid_date) + 7*30   end as date_7_inst
	,case when its.number_of_installments >= 8 then date(its.paid_date) + 8*30   end as date_8_inst
	,case when its.number_of_installments >= 9 then date(its.paid_date) + 9*30   end as date_9_inst
	,case when its.number_of_installments >= 10 then date(its.paid_date) + 10*30 end as date_10_inst
	,sum(coalesce(its.total_gmv,0)/its.number_of_installments) inst_gmv

from star_schema.fact_item_sold its
	left join star_schema.dim_company cp 		on its.fk_company = cp.id_company
	left join star_schema.dim_store st 			on st.fk_company = cp.id_company
	left join star_schema.dim_payment_method py on its.fk_payment_method = py.id_payment_method
	left join star_schema.dim_date dt 			on its.fk_order_date = dt.id_date
where 1=1
	and st.is_marketplace_out = 0
	and dt.date_ between (sysdate - 64) and (sysdate - 8) --
	and paid_date is not null
	and py.payment_method_group = 'CREDIT CARD'
	and cp.company_code = 'DBR'
	and datediff(day, its.order_date, its.paid_date) between 0 and 8 -- max relevant leadtime (>=0.9%)
group by
	cp.company_name
	,its.number_of_installments
	,case when its.number_of_installments >= 1 then date(its.paid_date) + 1*30   end 
	,case when its.number_of_installments >= 2 then date(its.paid_date) + 2*30   end 
	,case when its.number_of_installments >= 3 then date(its.paid_date) + 3*30   end 
	,case when its.number_of_installments >= 4 then date(its.paid_date) + 4*30   end 
	,case when its.number_of_installments >= 5 then date(its.paid_date) + 5*30   end 
	,case when its.number_of_installments >= 6 then date(its.paid_date) + 6*30   end 
	,case when its.number_of_installments >= 7 then date(its.paid_date) + 7*30   end 
	,case when its.number_of_installments >= 8 then date(its.paid_date) + 8*30   end 
	,case when its.number_of_installments >= 9 then date(its.paid_date) + 9*30   end 
	,case when its.number_of_installments >= 10 then date(its.paid_date) + 10*30 end 