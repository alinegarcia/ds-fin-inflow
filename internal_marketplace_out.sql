with dbr_trikan as (
select 
	aux1.company_name, aux1.store_name, 
	--aux1.order_weekday, -- doesn't matter
	aux1.customer_payment_leadtime_days, 
	aux1.sample_qty, aux2.sample_total_qty, (aux1.sample_qty/aux2.sample_total_qty::float) expected_internal_mktplace_out_share
from
	(
	select 
	  --dt.day_of_week  								as order_weekday
	 cp.company_name 								as company_name 
	, st.store_name 								as store_name
	, datediff(day, its.order_date, its.paid_date) 	as customer_payment_leadtime_days
	, count(distinct(order_number)) 				as sample_qty
	from star_schema.fact_item_sold its
		left join star_schema.dim_company cp 		on its.fk_company = cp.id_company
		left join star_schema.dim_store st 			on st.id_store = its.fk_store
		left join star_schema.dim_date dt 			on its.fk_order_date = dt.id_date
	where 1=1
		and st.is_internal_marketplace_out = 1 
		--and cp.company_code = 'DBR'
		--and st.store_name in ('TRICAE','KANUI')
		and dt.date_ between (sysdate - 64) and (sysdate - 7) --
		and paid_date is not null
		and datediff(day, its.order_date, its.paid_date) <= 6 -- max relevant leadtime (>=0.9%)
	group by
	  --dt.day_of_week 
	 cp.company_name 							
	, st.store_name
	, datediff(day, its.order_date, its.paid_date)
	) aux1
join 
	(
	select 
	  --dt.day_of_week  								as order_weekday
	 cp.company_name 								as company_name
	, st.store_name 								as store_name
	-- , datediff(day, its.order_date, its.paid_date) 	as processing_days
	, count(distinct(order_number))					as sample_total_qty
	from star_schema.fact_item_sold its
		left join star_schema.dim_company cp 		on its.fk_company = cp.id_company
		left join star_schema.dim_store st 			on st.id_store = its.fk_store 
		left join star_schema.dim_date dt 			on its.fk_order_date = dt.id_date
	where 1=1
		and st.is_internal_marketplace_out = 1 
		--and cp.company_code = 'DBR'
		--and st.store_name in ('TRICAE','KANUI')
		and dt.date_ between (sysdate - 64) and (sysdate - 7) --
		and paid_date is not null
		and datediff(day, its.order_date, its.paid_date) <= 6 -- max relevant leadtime (>=0.9%)
	group by
	  --dt.day_of_week 
	 cp.company_name 							
	, st.store_name 					
	-- , datediff(day, its.order_date, its.paid_date)
	) aux2   on aux1.company_name = aux2.company_name
				and aux1.store_name = aux2.store_name
				--and aux1.order_weekday = aux2.order_weekday
),

rule_inflow_date as (
select 
	aux1.paid_date 	as paid_date
	,aux2.inflow_date 	as inflow_date
from 
(select 
	date(dt.date_)	as paid_date
	,case 
		when (extract(d from dt.date_) <= 10) 
			then extract(y from dt.date_)::VARCHAR(10) || extract(mon from dt.date_)::VARCHAR(10)
		else extract(y from dateadd(month,1,dt.date_))::VARCHAR(10) 
				|| extract(mon from dateadd(month,1,dt.date_))::VARCHAR(10)
	 end as year_month
from star_schema.dim_date dt
where 1 
	and dt.iso_date between (sysdate - 95) and (sysdate + 150)) aux1

left join 

(select 
	extract(y from dt.date_)::VARCHAR(10) 
		||  extract(mon from dt.date_)::VARCHAR(10) as year_month 
	,max(dt.date_) as inflow_date
	from star_schema.dim_date dt
	where 1
		and dt.day_of_week not in (6,7)
		and dt.iso_date between (sysdate - 95) and (sysdate + 60)
	group by extract(y from dt.date_)::VARCHAR(10) 
			||  extract(mon from dt.date_)::VARCHAR(10)
) aux2 on aux1.year_month = aux2.year_month)


---------------------------

(select 
	 aux.order_date 
	,aux.customer_paid_date_est
	,dtr.inflow_date					as inflow_date_est
	--,aux.order_weekday
	,aux.company_name
	,aux.store_name
	,total_gmv_bef_cnc 					as total_gmv_bef_cnc
	,total_gmv_aft_cnc_est 			 	as total_gmv_aft_cnc_est
	,aux.total_gmv_aft_cnc_est*(1-0.07) as total_gmv_commission_aft_cnc_est
from (select 
	  dt.date_ 											as order_date
	, case 
		when dt.date_ < (sysdate - 7) then date(its.paid_date) 
		else (dt.date_ + cf.customer_payment_leadtime_days) 
	 end	 											as customer_paid_date_est
	--, dt.day_of_week  									as order_weekday
	, cp.company_name 									as company_name
	, st.store_name										as store_name
	-- , its.order_number								as order_number
	, sum(coalesce(its.total_gmv, 0)*(cf.expected_internal_mktplace_out_share))		as total_gmv_bef_cnc
	, sum(case 
		when dt.date_ < (sysdate - 7) then	-- actual
			case
				when its.is_paid = 1 then coalesce(its.total_gmv, 0)
				else 0
			end
		else								-- forecast
		(1 - its.bef_paid_cancelled_estimated_rate
				 )*(coalesce(its.total_gmv, 0))
	    end * (cf.expected_internal_mktplace_out_share)) 							as total_gmv_aft_cnc_est
	
	from star_schema.fact_item_sold its
		left join star_schema.dim_country ct 							on its.fk_sale_country = ct.id_country
		left join star_schema.dim_company cp 							on its.fk_company = cp.id_company
		left join star_schema.dim_store st 								on its.fk_store = st.id_store
		left join star_schema.dim_device_platform dp 					on its.fk_device_platform = dp.id_device_platform
		left join star_schema.dim_channel_partner_google_analytics cpga on its.fk_last_click_channel_partner_google_analytics = cpga.id_channel_partner_google_analytics
		left join star_schema.dim_product_config pdc 					on its.fk_product_config = pdc.id_product_config
		left join star_schema.dim_date dt 								on its.fk_order_date = dt.id_date
		left join dbr_trikan cf  										on cp.company_name = cf.company_name
																			and st.store_name = cf.store_name
																			--and dt.day_of_week = cf.order_weekday
	where 1=1
		and st.is_internal_marketplace_out = 1 
		and its.is_invalid = 0
		--and cp.company_code = 'DBR'
		--and st.store_name in ('TRICAE','KANUI')
		and dt.date_ between (sysdate - 95) and (sysdate - 1) 
	group by
		dt.date_
		, case 
			when dt.date_ < (sysdate - 7) then date(its.paid_date) 
			else (dt.date_ + cf.customer_payment_leadtime_days) 
		 end
		--, dt.day_of_week
		, cp.company_name
		, st.store_name

) aux

left join rule_inflow_date dtr 									on dtr.paid_date = aux.customer_paid_date_est

where 
	datediff(day, aux.order_date, coalesce(aux.customer_paid_date_est, aux.order_date)) >= 0 -- detected erros in DB, where order_date > paid_date. insignificant # occurrences.
)										

