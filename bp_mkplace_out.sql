-- BP - Marketplace Out

with store as (
select 
 cp.company_name 									as company_name
, cast(sum(case when st.store_name = 'B2W' then coalesce(its.total_gmv,0) end)
	/sum(coalesce(its.total_gmv,0)) as double precision) as b2w_share
, cast(sum(case when st.store_name = 'EXTERNALSHOP' then coalesce(its.total_gmv,0) end)
	/sum(coalesce(its.total_gmv,0)) as double precision) as walmart_share
, cast(sum(case when st.store_name = 'EXTRA' then coalesce(its.total_gmv,0) end)
	/sum(coalesce(its.total_gmv,0)) as double precision) as extra_share
from star_schema.fact_item_sold its
	left join star_schema.dim_store st on its.fk_store = st.id_store
	left join star_schema.dim_company cp on its.fk_company = cp.id_company
	left join star_schema.dim_date dt on its.fk_order_date = dt.id_date
where 1=1
	and dt.date_ between (sysdate - 64) and (sysdate - 7)
	and cp.company_code = 'TRI'
	and its.is_invalid = 0
	and st.is_marketplace_out = 1
	and coalesce(its.total_gmv,0) > 0
group by
  cp.company_name
),

  
cnc as (
select
  cp.company_name 									as company_name
, 1 - sum(case when is_paid = 1 then coalesce(its.total_gmv,0)
  - coalesce(its.refund_product_value, 0) 
  - coalesce(its.refund_shipping_value, 0)
  - coalesce(its.refund_gift_wrap_value, 0) else 0 end)/
  sum(coalesce(its.total_gmv,0)
  - coalesce(its.refund_product_value, 0) 
  - coalesce(its.refund_shipping_value, 0)
  - coalesce(its.refund_gift_wrap_value, 0)) 		as cancellation_rate
, 1 - sum(case when is_paid = 1 and st.store_name = 'B2W' then coalesce(its.total_gmv,0)
  - coalesce(its.refund_product_value, 0) 
  - coalesce(its.refund_shipping_value, 0)
  - coalesce(its.refund_gift_wrap_value, 0) else 0 end)/
  sum(case when st.store_name = 'B2W' then coalesce(its.total_gmv,0)
  - coalesce(its.refund_product_value, 0) 
  - coalesce(its.refund_shipping_value, 0)
  - coalesce(its.refund_gift_wrap_value, 0) end) 	as cancellation_rate_b2w
, 1 - sum(case when is_paid = 1 and st.store_name = 'EXTERNALSHOP' then coalesce(its.total_gmv,0)
  - coalesce(its.refund_product_value, 0) 
  - coalesce(its.refund_shipping_value, 0)
  - coalesce(its.refund_gift_wrap_value, 0) else 0 end)/
  sum(case when st.store_name = 'EXTERNALSHOP' then coalesce(its.total_gmv,0)
  - coalesce(its.refund_product_value, 0) 
  - coalesce(its.refund_shipping_value, 0)
  - coalesce(its.refund_gift_wrap_value, 0) end) 	as cancellation_rate_walmart
  , 1 - sum(case when is_paid = 1 and st.store_name = 'EXTRA' then coalesce(its.total_gmv,0)
  - coalesce(its.refund_product_value, 0) 
  - coalesce(its.refund_shipping_value, 0)
  - coalesce(its.refund_gift_wrap_value, 0) else 0 end)/
  sum(case when st.store_name = 'EXTRA' then coalesce(its.total_gmv,0)
  - coalesce(its.refund_product_value, 0) 
  - coalesce(its.refund_shipping_value, 0)
  - coalesce(its.refund_gift_wrap_value, 0) end) 	as cancellation_rate_extra
from star_schema.fact_item_sold its
	left join star_schema.dim_store st 			on its.fk_store = st.id_store
	left join star_schema.dim_company cp 		on its.fk_company = cp.id_company
	left join star_schema.dim_date dt 			on its.fk_order_date = dt.id_date
where 1=1
	and dt.date_ between (sysdate - 64) and (sysdate - 7)
	and cp.company_code = 'TRI'
	and its.is_invalid = 0
	and st.is_marketplace_out = 1
	and coalesce(its.total_gmv,0) > 0
group by
  cp.company_name
),
  
   
  
b2w_paid_date as (
select 
	aux1.company_name, aux1.customer_payment_leadtime_days, aux1.sample_qty, 
	aux2.sample_total_qty, (aux1.sample_qty/aux2.sample_total_qty::float) expected_pay_leadtime_share
from
	(
	select 
	  --dt.day_of_week  								as order_weekday
	  cp.company_name 								as company_name
	, datediff(day, its.order_date, its.paid_date) 	as customer_payment_leadtime_days
	, count(distinct(its.src_bob_fk_sales_order_item)) 				as sample_qty
	from star_schema.fact_item_sold its
		left join star_schema.dim_company cp 		on its.fk_company = cp.id_company
		left join star_schema.dim_store st 			on st.fk_company = cp.id_company
		left join star_schema.dim_date dt 			on its.fk_order_date = dt.id_date
	where 1=1
		and st.is_marketplace_out = 1
		and st.store_name = 'B2W'
		and dt.date_ between (sysdate - 64) and (sysdate - 7) --
		and paid_date is not null
		and datediff(day, its.order_date, its.paid_date) between 0 and 6 -- max relevant leadtime (>=0.9%)
	group by
	  --dt.day_of_week 
	 cp.company_name 							
	, datediff(day, its.order_date, its.paid_date)
	) aux1
join 
	(
	select 
	  --dt.day_of_week  								as order_weekday
	 cp.company_name 								as company_name
	-- , datediff(day, its.order_date, its.paid_date) 	as processing_days
	, count(distinct(its.src_bob_fk_sales_order_item))					as sample_total_qty
	from star_schema.fact_item_sold its
		left join star_schema.dim_company cp 		on its.fk_company = cp.id_company
		left join star_schema.dim_store st 			on st.fk_company = cp.id_company
		left join star_schema.dim_date dt 			on its.fk_order_date = dt.id_date
	where 1=1
		and st.is_marketplace_out = 1
		and st.store_name = 'B2W'
		and dt.date_ between (sysdate - 64) and (sysdate - 7)
		and paid_date is not null
		and datediff(day, its.order_date, its.paid_date) between 0 and 6 
	group by
	  --dt.day_of_week 
	 cp.company_name 							
	-- , datediff(day, its.order_date, its.paid_date)
	) aux2   on --aux1.order_weekday = aux2.order_weekday
				aux1.company_name = aux2.company_name
),

walmart_paid_date as (
select 
	aux1.company_name, aux1.customer_payment_leadtime_days, aux1.sample_qty, 
	aux2.sample_total_qty, (aux1.sample_qty/aux2.sample_total_qty::float) expected_pay_leadtime_share
from
	(
	select 
	  --dt.day_of_week  								as order_weekday
	 cp.company_name 								as company_name
	, datediff(day, its.order_date, its.paid_date) 	as customer_payment_leadtime_days
	, count(distinct(its.src_bob_fk_sales_order_item)) 				as sample_qty
	from star_schema.fact_item_sold its
		left join star_schema.dim_company cp 		on its.fk_company = cp.id_company
		left join star_schema.dim_store st 			on st.fk_company = cp.id_company
		left join star_schema.dim_date dt 			on its.fk_order_date = dt.id_date
	where 1=1
		and st.is_marketplace_out = 1
		and st.store_name = 'EXTERNALSHOP'
		and dt.date_ between (sysdate - 64) and (sysdate - 7) --
		and paid_date is not null
		and datediff(day, its.order_date, its.paid_date) between 0 and 6 -- max relevant leadtime (>=0.9%)
	group by
	  --dt.day_of_week 
	 cp.company_name 							
	, datediff(day, its.order_date, its.paid_date)
	) aux1
join 
	(
	select 
	  --dt.day_of_week  								as order_weekday
	 cp.company_name 								as company_name
	--, st.store_name									as store_name
	-- , datediff(day, its.order_date, its.paid_date) 	as processing_days
	, count(distinct(its.src_bob_fk_sales_order_item))					as sample_total_qty
	from star_schema.fact_item_sold its
		left join star_schema.dim_company cp 		on its.fk_company = cp.id_company
		left join star_schema.dim_store st 			on st.fk_company = cp.id_company
		left join star_schema.dim_date dt 			on its.fk_order_date = dt.id_date
	where 1=1
		and st.is_marketplace_out = 1
		and st.store_name = 'EXTERNALSHOP'
		and dt.date_ between (sysdate - 64) and (sysdate - 7)
		and paid_date is not null
		and datediff(day, its.order_date, its.paid_date) between 0 and 6 
	group by
	  --dt.day_of_week 
	 cp.company_name 							
	-- , datediff(day, its.order_date, its.paid_date)
	) aux2   on --aux1.order_weekday = aux2.order_weekday
				 aux1.company_name = aux2.company_name
),


extra_paid_date as (
select 
	aux1.company_name, aux1.customer_payment_leadtime_days, aux1.sample_qty, 
	aux2.sample_total_qty, (aux1.sample_qty/aux2.sample_total_qty::float) expected_pay_leadtime_share
from
	(
	select 
	  --dt.day_of_week  								as order_weekday
	 cp.company_name 								as company_name
	, datediff(day, its.order_date, its.paid_date) 	as customer_payment_leadtime_days
	, count(distinct(its.src_bob_fk_sales_order_item)) 				as sample_qty
	from star_schema.fact_item_sold its
		left join star_schema.dim_company cp 		on its.fk_company = cp.id_company
		left join star_schema.dim_store st 			on st.fk_company = cp.id_company
		left join star_schema.dim_date dt 			on its.fk_order_date = dt.id_date
	where 1=1
		and st.is_marketplace_out = 1
		and st.store_name = 'EXTRA'
		and dt.date_ between (sysdate - 64) and (sysdate - 7) --
		and paid_date is not null
		and datediff(day, its.order_date, its.paid_date) between 0 and 6 -- max relevant leadtime (>=0.9%)
	group by
	  --dt.day_of_week 
	 cp.company_name 							
	, datediff(day, its.order_date, its.paid_date)
	) aux1
join 
	(
	select 
	  --dt.day_of_week  								as order_weekday
	 cp.company_name 								as company_name
	-- , datediff(day, its.order_date, its.paid_date) 	as processing_days
	, count(distinct(its.src_bob_fk_sales_order_item))					as sample_total_qty
	from star_schema.fact_item_sold its
		left join star_schema.dim_company cp 		on its.fk_company = cp.id_company
		left join star_schema.dim_store st 			on st.fk_company = cp.id_company
		left join star_schema.dim_date dt 			on its.fk_order_date = dt.id_date
	where 1=1
		and st.is_marketplace_out = 1
		and st.store_name = 'EXTRA'
		and dt.date_ between (sysdate - 64) and (sysdate - 7)
		and paid_date is not null
		and datediff(day, its.order_date, its.paid_date) between 0 and 6 
	group by
	  --dt.day_of_week 
	 cp.company_name 							
	-- , datediff(day, its.order_date, its.paid_date)
	) aux2   on --aux1.order_weekday = aux2.order_weekday
				 aux1.company_name = aux2.company_name
),

exp_date_b2w as (select
	aux1.company_name
	,aux1.is_crossdocking
	,aux1.shipping_time_in_workdays
	,aux1.sample_qty
	,aux2.sample_total_qty
	,(aux1.sample_qty/aux2.sample_total_qty::float) as share_crossdocking

from (select
	 'TRICAE'                                                   as company_name
	 ,case when fk_catalog_shipment_type = 3 then 1 else 0 end 	as is_crossdocking
	 ,count(its.src_bob_fk_sales_order_item)                       as sample_qty
	 ,avg((case when fk_catalog_shipment_type = 3 then shipment_supplier_delivery_time else 0 end) 
		+ (soi.shipment_warehouse_processing_time)) 			as shipping_time_in_workdays			
	from raw_bob_tricae.sales_order_item soi
		left join star_schema.fact_item_sold its on its.src_bob_fk_sales_order_item = soi.id_sales_order_item
		left join star_schema.dim_store st		 on st.id_store = its.fk_store
	where 1=1
		and its.is_invalid = 0
		and st.is_marketplace_out = 1
		and soi.created_at between (sysdate - 95) and (sysdate - 1) 
		and st.store_name = 'B2W'

	group by 
		company_name
		,case when fk_catalog_shipment_type = 3 then 1 else 0 end
) aux1

join

(select
	 'TRICAE'                                                   as company_name
	 --,case when fk_catalog_shipment_type = 3 then 1 else 0 end 	as is_crossdocking
	 ,count(its.src_bob_fk_sales_order_item)                       as sample_total_qty
	from raw_bob_tricae.sales_order_item soi
		left join star_schema.fact_item_sold its on its.src_bob_fk_sales_order_item = soi.id_sales_order_item
		left join star_schema.dim_store st		 on st.id_store = its.fk_store
	where 1=1
		and its.is_invalid = 0
		and st.is_marketplace_out = 1
		and soi.created_at between (sysdate - 95) and (sysdate - 1) 
		and st.store_name = 'B2W'

	group by 
		company_name
) aux2 on aux1.company_name = aux2.company_name),


exp_date_extra as (select
	aux1.company_name
	,aux1.is_crossdocking
	,aux1.shipping_time_in_workdays
	,aux1.sample_qty
	,aux2.sample_total_qty
	,(aux1.sample_qty/aux2.sample_total_qty::float) as share_crossdocking

from (select
	 'TRICAE'                                                   as company_name
	 ,case when fk_catalog_shipment_type = 3 then 1 else 0 end 	as is_crossdocking
	 ,count(its.src_bob_fk_sales_order_item)                       as sample_qty
	 ,avg((case when fk_catalog_shipment_type = 3 then shipment_supplier_delivery_time else 0 end) 
		+ (soi.shipment_warehouse_processing_time)) 			as shipping_time_in_workdays			
	from raw_bob_tricae.sales_order_item soi
		left join star_schema.fact_item_sold its on its.src_bob_fk_sales_order_item = soi.id_sales_order_item
		left join star_schema.dim_store st		 on st.id_store = its.fk_store
	where 1=1
		and its.is_invalid = 0
		and st.is_marketplace_out = 1
		and soi.created_at between (sysdate - 95) and (sysdate - 1) 
		and st.store_name = 'EXTRA'

	group by 
		company_name
		,case when fk_catalog_shipment_type = 3 then 1 else 0 end
) aux1

join

(select
	 'TRICAE'                                                   as company_name
	 --,case when fk_catalog_shipment_type = 3 then 1 else 0 end 	as is_crossdocking
	 ,count(its.src_bob_fk_sales_order_item)                       as sample_total_qty
	from raw_bob_tricae.sales_order_item soi
		left join star_schema.fact_item_sold its on its.src_bob_fk_sales_order_item = soi.id_sales_order_item
		left join star_schema.dim_store st		 on st.id_store = its.fk_store
	where 1=1
		and its.is_invalid = 0
		and st.is_marketplace_out = 1
		and soi.created_at between (sysdate - 95) and (sysdate - 1) 
		and st.store_name = 'EXTRA'

	group by 
		company_name
) aux2 on aux1.company_name = aux2.company_name),



exp_date_walmart as (select
	aux1.company_name
	,aux1.is_crossdocking
	,aux1.shipping_time_in_workdays
	,aux1.sample_qty
	,aux2.sample_total_qty
	,(aux1.sample_qty/aux2.sample_total_qty::float) as share_crossdocking

from (select
	 'TRICAE'                                                   as company_name
	 ,case when fk_catalog_shipment_type = 3 then 1 else 0 end 	as is_crossdocking
	 ,count(its.src_bob_fk_sales_order_item)                       as sample_qty
	 ,avg((case when fk_catalog_shipment_type = 3 then shipment_supplier_delivery_time else 0 end) 
		+ (soi.shipment_warehouse_processing_time)) 			as shipping_time_in_workdays			
	from raw_bob_tricae.sales_order_item soi
		left join star_schema.fact_item_sold its on its.src_bob_fk_sales_order_item = soi.id_sales_order_item
		left join star_schema.dim_store st		 on st.id_store = its.fk_store
	where 1=1
		and its.is_invalid = 0
		and st.is_marketplace_out = 1
		and soi.created_at between (sysdate - 95) and (sysdate - 1) 
		and st.store_name = 'EXTERNALSHOP'

	group by 
		company_name
		,case when fk_catalog_shipment_type = 3 then 1 else 0 end
) aux1

join

(select
	 'TRICAE'                                                   as company_name
	 --,case when fk_catalog_shipment_type = 3 then 1 else 0 end 	as is_crossdocking
	 ,count(its.src_bob_fk_sales_order_item)                       as sample_total_qty
	from raw_bob_tricae.sales_order_item soi
		left join star_schema.fact_item_sold its on its.src_bob_fk_sales_order_item = soi.id_sales_order_item
		left join star_schema.dim_store st		 on st.id_store = its.fk_store
	where 1=1
		and its.is_invalid = 0
		and st.is_marketplace_out = 1
		and soi.created_at between (sysdate - 95) and (sysdate - 1) 
		and st.store_name = 'EXTERNALSHOP'

	group by 
		company_name
) aux2 on aux1.company_name = aux2.company_name),


rule_expedition_date_walmart as (
select 
	aux1.expedition_date 	as expedition_date
	,aux2.inflow_date			as inflow_date

from (select 
	date(dt.date_) as expedition_date
	,case 
		when (extract(d from dt.date_) between 26 and 31) 
			then extract(y from dateadd(month,2,dt.date_))::VARCHAR(20) 
				|| extract(mon from dateadd(month,2,dt.date_))::VARCHAR(20)
		else
			extract(y from dateadd(month,1,dt.date_))::VARCHAR(20) 
				||  extract(mon from dateadd(month,1,dt.date_))::VARCHAR(20)
	end as year_month

	from star_schema.dim_date dt

	where dt.iso_date between (sysdate - 95) and (sysdate + 60)
) aux1

left join 

(select 
	dt.date_ as inflow_date
	,extract(y from dt.date_)::VARCHAR(20) ||  extract(mon from dt.date_)::VARCHAR(20) as year_month
from star_schema.dim_date dt
where 1
	and dt.day_of_week = '4' 
	and (dt.day_of_month::int) between 8 and 14
	and dt.iso_date between (sysdate - 95) and (sysdate + 150) 

) aux2

on aux2.year_month = aux1.year_month)


-------------------------------


select
	aux.*
	,case
		when (extract(d from aux.expedition_date) between 1 and 15) then to_char(dateadd(month,1,aux.expedition_date), 'YYYY-MM-01')::date
		else to_char(dateadd(month,1,aux.expedition_date), 'YYYY-MM-15')::date
	end 								as inflow_date
	,aux.total_gmv_aft_cnc*(1-0.13) as total_gmv_comm_aft_cnc

from 
(select 
  dt.date_ as order_date
, (dt.date_ + cf.customer_payment_leadtime_days) 	as customer_paid_date
, (dt.date_ + cf.customer_payment_leadtime_days 
	+ ed.shipping_time_in_workdays)					as expedition_date
--, dt.day_of_week  									as order_weekday
, ma.store_name as company_name 
, 'B2W' 	as store_name
, round(coalesce(ma.total_gmv_bef_cnc_bef_ret*ds.gmv_daily_share
	*st.b2w_share*cf.expected_pay_leadtime_share*ed.share_crossdocking,0),0) as total_gmv_bef_cnc
, round(coalesce(ma.total_gmv_bef_cnc_bef_ret*ds.gmv_daily_share
	*st.b2w_share*cf.expected_pay_leadtime_share*ed.share_crossdocking*(1- cn.cancellation_rate_b2w),0),0) as total_gmv_aft_cnc

from targets.tgt_mkt_monthly_absolute ma
	join targets.tgt_mkt_daily_share ds  on ma.country_name = ds.country_name
	         and ma.store_name = ds.store_name
	         and ma.year = ds.year
	         and ma.month_of_year = ds.month_of_year
	join star_schema.dim_currency curr 	on curr.currency_code = ma.currency_code
	join star_schema.dim_date dt 		on cast(concat(concat(ma.year, ma.month_of_year), ds.day_of_month) as bigint) = dt.id_date
	join store st 						on ma.store_name = st.company_name
	join cnc cn 						on ma.store_name = cn.company_name
	join b2w_paid_date cf 				on ma.store_name = cf.company_name
											--and dt.day_of_week = cf.order_weekday
	join exp_date_b2w ed 				on ma.store_name = ed.company_name 
	

where 1=1
	and ma.country_name = 'BRAZIL'
	and ma.store_name = 'TRICAE') aux

where 
	datediff(day, aux.order_date, coalesce(aux.customer_paid_date, aux.order_date)) >= 0 -- detected erros in DB, where order_date > paid_date. insignificant # occurrences.



UNION ALL



select
	aux.*
	,case 
		when (extract(d from aux.expedition_date) between 5 and 15) then to_char(aux.expedition_date, 'YYYY-MM-25')::date
		when (extract(d from aux.expedition_date) between 16 and 25) then to_char(dateadd(month,1,aux.expedition_date), 'YYYY-MM-05')::date
		else 
			case 
				when extract(d from aux.expedition_date) between 26 and 31 then to_char(dateadd(month,1,aux.expedition_date), 'YYYY-MM-15')::date
				when extract(d from aux.expedition_date) between 1 and 4 then to_char(aux.expedition_date, 'YYYY-MM-15')::date
			end 
	end 								as inflow_date
	,aux.total_gmv_aft_cnc*(1-0.125) as total_gmv_comm_aft_cnc

from 
(select 
  dt.date_ as order_date
, (dt.date_ + cf.customer_payment_leadtime_days) 	as customer_paid_date
, (dt.date_ + cf.customer_payment_leadtime_days 
	+ ed.shipping_time_in_workdays)					as expedition_date
--, dt.day_of_week  									as order_weekday
, ma.store_name as company_name 
, 'EXTRA' 	as store_name
, round(coalesce(ma.total_gmv_bef_cnc_bef_ret*ds.gmv_daily_share
	*st.extra_share*cf.expected_pay_leadtime_share*ed.share_crossdocking,0),0) as total_gmv_bef_cnc
, round(coalesce(ma.total_gmv_bef_cnc_bef_ret*ds.gmv_daily_share
	*st.extra_share*cf.expected_pay_leadtime_share*ed.share_crossdocking*(1- cn.cancellation_rate_b2w),0),0) as total_gmv_aft_cnc

from targets.tgt_mkt_monthly_absolute ma
	join targets.tgt_mkt_daily_share ds  on ma.country_name = ds.country_name
	         and ma.store_name = ds.store_name
	         and ma.year = ds.year
	         and ma.month_of_year = ds.month_of_year
	join star_schema.dim_currency curr 	on curr.currency_code = ma.currency_code
	join star_schema.dim_date dt 		on cast(concat(concat(ma.year, ma.month_of_year), ds.day_of_month) as bigint) = dt.id_date
	join store st 						on ma.store_name = st.company_name
	join cnc cn 						on ma.store_name = cn.company_name
	join extra_paid_date cf 			on ma.store_name = cf.company_name
											--and dt.day_of_week = cf.order_weekday
	join exp_date_extra ed 				on ma.store_name = ed.company_name 
	

where 1=1
	and ma.country_name = 'BRAZIL'
	and ma.store_name = 'TRICAE') aux

where 
	datediff(day, aux.order_date, coalesce(aux.customer_paid_date, aux.order_date)) >= 0



UNION ALL


select
	aux.*
	,to_char(re.inflow_date, 'YYYY-MM-DD')::date						as inflow_date
	,aux.total_gmv_aft_cnc*(1-0.125) as total_gmv_comm_aft_cnc

from 
(select 
  dt.date_ as order_date
, (dt.date_ + cf.customer_payment_leadtime_days) 	as customer_paid_date
, (dt.date_ + cf.customer_payment_leadtime_days 
	+ ed.shipping_time_in_workdays)					as expedition_date
--, dt.day_of_week  									as order_weekday
, ma.store_name as company_name 
, 'EXTERNALSHOP' 	as store_name
, round(coalesce(ma.total_gmv_bef_cnc_bef_ret*ds.gmv_daily_share
	*st.walmart_share*cf.expected_pay_leadtime_share*ed.share_crossdocking,0),0) as total_gmv_bef_cnc
, round(coalesce(ma.total_gmv_bef_cnc_bef_ret*ds.gmv_daily_share
	*st.walmart_share*cf.expected_pay_leadtime_share*ed.share_crossdocking*(1- cn.cancellation_rate_b2w),0),0) as total_gmv_aft_cnc

from targets.tgt_mkt_monthly_absolute ma
	join targets.tgt_mkt_daily_share ds  on ma.country_name = ds.country_name
	         and ma.store_name = ds.store_name
	         and ma.year = ds.year
	         and ma.month_of_year = ds.month_of_year
	join star_schema.dim_currency curr 		on curr.currency_code = ma.currency_code
	join star_schema.dim_date dt 			on cast(concat(concat(ma.year, ma.month_of_year), ds.day_of_month) as bigint) = dt.id_date
	join store st 							on ma.store_name = st.company_name
	join cnc cn 							on ma.store_name = cn.company_name
	join walmart_paid_date cf 				on ma.store_name = cf.company_name
											--and dt.day_of_week = cf.order_weekday
	join exp_date_walmart ed 				on ma.store_name = ed.company_name 
	
where 1=1
	and ma.country_name = 'BRAZIL'
	and ma.store_name = 'TRICAE') aux

join rule_expedition_date_walmart re 	on re.expedition_date = aux.expedition_date

where 
	datediff(day, aux.order_date, coalesce(aux.customer_paid_date, aux.order_date)) >= 0