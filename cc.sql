with credit_card as (
select 
	aux1.company_name, aux1.payment_method_group, aux1.order_weekday, aux1.customer_payment_leadtime_days, 
	aux1.sample_qty, aux2.sample_total_qty, (aux1.sample_qty/aux2.sample_total_qty::float) expected_payment_share
from
	(
	select 
	  dt.day_of_week  								as order_weekday
	, cp.company_name 								as company_name
	, py.payment_method_group						as payment_method_group
	, datediff(day, its.order_date, its.paid_date) 	as customer_payment_leadtime_days
	, count(distinct(order_number)) 				as sample_qty
	from star_schema.fact_item_sold its
		left join star_schema.dim_company cp 		on its.fk_company = cp.id_company
		left join star_schema.dim_payment_method py on its.fk_payment_method = py.id_payment_method
		left join star_schema.dim_date dt 			on its.fk_order_date = dt.id_date
	where 1=1
		and dt.date_ between (sysdate - 64) and (sysdate - 8)
		and paid_date is not null
		and py.payment_method_group = 'CREDIT CARD'
		and cp.company_code in ('DBR','TRI','KAN')
		and datediff(day, its.order_date, its.paid_date) <= 2 -- max relevant leadtime (>=0.9%)
	group by
	  dt.day_of_week 
	, cp.company_name 							
	, py.payment_method_group 					
	, datediff(day, its.order_date, its.paid_date)
	) aux1
join 
	(
	select 
	  dt.day_of_week  								as order_weekday
	, cp.company_name 								as company_name
	, py.payment_method_group						as payment_method_group
	-- , datediff(day, its.order_date, its.paid_date) 	as processing_days
	, count(distinct(order_number))					as sample_total_qty
	from star_schema.fact_item_sold its
		left join star_schema.dim_company cp 		on its.fk_company = cp.id_company
		left join star_schema.dim_payment_method py on its.fk_payment_method = py.id_payment_method
		left join star_schema.dim_date dt 			on its.fk_order_date = dt.id_date
	where 1=1
		and dt.date_ between (sysdate - 64) and (sysdate - 8)
		and paid_date is not null
		and py.payment_method_group = 'CREDIT CARD'
		and cp.company_code in ('DBR','TRI','KAN')
		and datediff(day, its.order_date, its.paid_date) <= 2 
	group by
	  dt.day_of_week 
	, cp.company_name 							
	, py.payment_method_group 					
	-- , datediff(day, its.order_date, its.paid_date)
	) aux2   on aux1.order_weekday = aux2.order_weekday
				and aux1.company_name = aux2.company_name
				and aux1.payment_method_group = aux2.payment_method_group
)


-- Credit Card - Outright and Marketplace In
select aux.*, (aux.customer_paid_date_est + aux.money_inflow_leadtime) as inflow_date_est
from (select 
	  dt.date_ 																												as order_date
	, case  
		when dt.date_ < (sysdate - 3) then date(its.paid_date) else (dt.date_ + cf.customer_payment_leadtime_days)  
	 end	 																												as customer_paid_date_est

	, case 
		when to_char((case when dt.date_ < (sysdate - 3) then date(its.paid_date) else (dt.date_ + cf.customer_payment_leadtime_days) end), 'd') in ('1','2','3','4','5') then 1
		when to_char((case when dt.date_ < (sysdate - 3) then date(its.paid_date) else (dt.date_ + cf.customer_payment_leadtime_days) end), 'd') = '6' then 3
		when to_char((case when dt.date_ < (sysdate - 3) then date(its.paid_date) else (dt.date_ + cf.customer_payment_leadtime_days) end), 'd') = '7' then 2
		else 0
	 end 																													as money_inflow_leadtime
	, dt.day_of_week  									as order_weekday
	, cp.company_name 									as company_name
	, st.store_name										as store_name
	-- , its.order_number								as order_number
	, py.payment_method_group 							as payment_method_group
	, py.gateway										as gateway
	, py.acquirer										as acquirer
	, py.cc_brand										as cc_brand
	, sum(coalesce(its.total_gmv, 0)*(cf.expected_payment_share))															as total_gmv_bef_cnc
	, sum((coalesce(its.total_gmv, 0) 
		- coalesce(its.refund_product_value, 0) 
		- coalesce(its.refund_shipping_value, 0)
		- coalesce(its.refund_gift_wrap_value, 0))*(cf.expected_payment_share))												as total_gmv_less_refund_voucher_bef_cnc
	, sum(case 
		when dt.date_ < (sysdate - 3) then	-- actual
			case
				when its.is_paid = 1 then (coalesce(its.total_gmv, 0) - coalesce(its.refund_product_value, 0) 
							- coalesce(its.refund_shipping_value, 0) - coalesce(its.refund_gift_wrap_value, 0))
				else 0
			end
		else 							  	-- forecast
		(1 - its.bef_paid_cancelled_estimated_rate)*(coalesce(its.total_gmv, 0) - coalesce(its.refund_product_value, 0) - coalesce(its.refund_shipping_value, 0) 
			- coalesce(its.refund_gift_wrap_value, 0)) end * (cf.expected_payment_share)) 									as total_gmv_less_refund_voucher_aft_cnc_est
	from star_schema.fact_item_sold its
		left join star_schema.dim_country ct 							on its.fk_sale_country = ct.id_country
		left join star_schema.dim_company cp 							on its.fk_company = cp.id_company
		left join star_schema.dim_store st 								on its.fk_store = st.id_store
		left join star_schema.dim_payment_method py 					on its.fk_payment_method = py.id_payment_method
		left join star_schema.dim_device_platform dp 					on its.fk_device_platform = dp.id_device_platform
		left join star_schema.dim_channel_partner_google_analytics cpga on its.fk_last_click_channel_partner_google_analytics = cpga.id_channel_partner_google_analytics
		left join star_schema.dim_product_config pdc 					on its.fk_product_config = pdc.id_product_config
		left join mapping.map_company_store mcp 						on its.fk_company = mcp.fk_company and st.store_name = mcp.store_name
		left join star_schema.dim_date dt 								on its.fk_order_date = dt.id_date
		left join credit_card cf  										on cp.company_name = cf.company_name
																			and py.payment_method_group = cf.payment_method_group
																			and dt.day_of_week = cf.order_weekday
	where 1=1
		and mcp.is_marketplace_out = 0 -- no marketplace out
		and its.is_invalid = 0
		and py.payment_method_group = 'CREDIT CARD'
		and cp.company_code in ('DBR','TRI','KAN')
		and dt.date_ between (sysdate - 95) and (sysdate - 1) 
	group by
	  dt.date_
	, case  
		when dt.date_ < (sysdate - 3) then date(its.paid_date) else (dt.date_ + cf.customer_payment_leadtime_days)  
	 end	 																												
	, case 
		when to_char((case when dt.date_ < (sysdate - 3) then date(its.paid_date) else (dt.date_ + cf.customer_payment_leadtime_days) end), 'd') in ('1','2','3','4','5') then 1
		when to_char((case when dt.date_ < (sysdate - 3) then date(its.paid_date) else (dt.date_ + cf.customer_payment_leadtime_days) end), 'd') = '6' then 3
		when to_char((case when dt.date_ < (sysdate - 3) then date(its.paid_date) else (dt.date_ + cf.customer_payment_leadtime_days) end), 'd') = '7' then 2
		else 0
	 end
	, dt.day_of_week
	, cp.company_name
	, st.store_name
	-- , its.order_number
	, py.payment_method_group
	, py.gateway
	, py.acquirer
	, py.cc_brand) aux