-- BP - Internal Marketplace Out

with store as (
select 
 cp.company_name 									as company_name
, cast(sum(case when st.store_name = 'TRICAE' then coalesce(its.total_gmv,0) end)
	/sum(coalesce(its.total_gmv,0)) as double precision) as tricae_share
, cast(sum(case when st.store_name = 'KANUI' then coalesce(its.total_gmv,0) end)
	/sum(coalesce(its.total_gmv,0)) as double precision) as kanui_share
from star_schema.fact_item_sold its
	left join star_schema.dim_store st on its.fk_store = st.id_store
	left join star_schema.dim_company cp on its.fk_company = cp.id_company
	left join star_schema.dim_date dt on its.fk_order_date = dt.id_date
where 1=1
	and dt.date_ between (sysdate - 64) and (sysdate - 7)
	and cp.company_code = 'DBR'
	and st.store_name in ('TRICAE', 'KANUI')
	and its.is_invalid = 0
	and st.is_internal_marketplace_out = 1
	and coalesce(its.total_gmv,0) > 0
group by
  cp.company_name
  ),

  
cnc as (
select
  cp.company_name 									as company_name
, 1 - sum(case when is_paid = 1 then coalesce(its.total_gmv,0)
  - coalesce(its.refund_product_value, 0) 
  - coalesce(its.refund_shipping_value, 0)
  - coalesce(its.refund_gift_wrap_value, 0) else 0 end)/
  sum(coalesce(its.total_gmv,0)
  - coalesce(its.refund_product_value, 0) 
  - coalesce(its.refund_shipping_value, 0)
  - coalesce(its.refund_gift_wrap_value, 0)) 		as cancellation_rate
, 1 - sum(case when is_paid = 1 and st.store_name = 'TRICAE' then coalesce(its.total_gmv,0)
  - coalesce(its.refund_product_value, 0) 
  - coalesce(its.refund_shipping_value, 0)
  - coalesce(its.refund_gift_wrap_value, 0) else 0 end)/
  sum(case when st.store_name = 'TRICAE' then coalesce(its.total_gmv,0)
  - coalesce(its.refund_product_value, 0) 
  - coalesce(its.refund_shipping_value, 0)
  - coalesce(its.refund_gift_wrap_value, 0) end) 	as cancellation_rate_tricae
, 1 - sum(case when is_paid = 1 and st.store_name = 'KANUI' then coalesce(its.total_gmv,0)
  - coalesce(its.refund_product_value, 0) 
  - coalesce(its.refund_shipping_value, 0)
  - coalesce(its.refund_gift_wrap_value, 0) else 0 end)/
  sum(case when st.store_name = 'KANUI' then coalesce(its.total_gmv,0)
  - coalesce(its.refund_product_value, 0) 
  - coalesce(its.refund_shipping_value, 0)
  - coalesce(its.refund_gift_wrap_value, 0) end) 	as cancellation_rate_kanui
from star_schema.fact_item_sold its
	left join star_schema.dim_store st 			on its.fk_store = st.id_store
	left join star_schema.dim_company cp 		on its.fk_company = cp.id_company
	left join star_schema.dim_date dt 			on its.fk_order_date = dt.id_date
where 1=1
	and dt.date_ between (sysdate - 64) and (sysdate - 7)
	and cp.company_code = 'DBR'
	and st.store_name in ('TRICAE', 'KANUI')
	and its.is_invalid = 0
	and st.is_internal_marketplace_out = 1
	and coalesce(its.total_gmv,0) > 0
group by
  cp.company_name
  --,st.store_name
  ),
  
   
  
dbr_tricae_inflow as (
select 
	aux1.company_name, aux1.order_weekday, aux1.customer_payment_leadtime_days, aux1.sample_qty, 
	aux2.sample_total_qty, (aux1.sample_qty/aux2.sample_total_qty::float) expected_internal_mktplace_out_share
from
	(
	select 
	  dt.day_of_week  								as order_weekday
	, cp.company_name 								as company_name
	, datediff(day, its.order_date, its.paid_date) 	as customer_payment_leadtime_days
	, count(distinct(order_number)) 				as sample_qty
	from star_schema.fact_item_sold its
		left join star_schema.dim_company cp 		on its.fk_company = cp.id_company
		left join star_schema.dim_store st 			on st.fk_company = cp.id_company
		left join star_schema.dim_date dt 			on its.fk_order_date = dt.id_date
	where 1=1
		and st.is_internal_marketplace_out = 1
		and st.store_name = 'TRICAE'
		and dt.date_ between (sysdate - 64) and (sysdate - 7) --
		and paid_date is not null
		and datediff(day, its.order_date, its.paid_date) between 0 and 6 -- max relevant leadtime (>=0.9%)
	group by
	  dt.day_of_week 
	, cp.company_name 							
	, datediff(day, its.order_date, its.paid_date)
	) aux1
join 
	(
	select 
	  dt.day_of_week  								as order_weekday
	, cp.company_name 								as company_name
	--, st.store_name									as store_name
	-- , datediff(day, its.order_date, its.paid_date) 	as processing_days
	, count(distinct(order_number))					as sample_total_qty
	from star_schema.fact_item_sold its
		left join star_schema.dim_company cp 		on its.fk_company = cp.id_company
		left join star_schema.dim_store st 			on st.fk_company = cp.id_company
		left join star_schema.dim_date dt 			on its.fk_order_date = dt.id_date
	where 1=1
		and st.is_internal_marketplace_out = 1
		and st.store_name = 'TRICAE'
		and dt.date_ between (sysdate - 64) and (sysdate - 7)
		and paid_date is not null
		and datediff(day, its.order_date, its.paid_date) between 0 and 6 
	group by
	  dt.day_of_week 
	, cp.company_name 							
	--, st.store_name	
	-- , datediff(day, its.order_date, its.paid_date)
	) aux2   on aux1.order_weekday = aux2.order_weekday
				and aux1.company_name = aux2.company_name
				--and aux1.store_name = aux2.store_name
),

dbr_kanui_inflow as (
select 
	aux1.company_name, aux1.order_weekday, aux1.customer_payment_leadtime_days, aux1.sample_qty, 
	aux2.sample_total_qty, (aux1.sample_qty/aux2.sample_total_qty::float) expected_internal_mktplace_out_share
from
	(
	select 
	  dt.day_of_week  								as order_weekday
	, cp.company_name 								as company_name
	--, st.store_name									as store_name
	, datediff(day, its.order_date, its.paid_date) 	as customer_payment_leadtime_days
	, count(distinct(order_number)) 				as sample_qty
	from star_schema.fact_item_sold its
		left join star_schema.dim_company cp 		on its.fk_company = cp.id_company
		left join star_schema.dim_store st 			on st.fk_company = cp.id_company
		left join star_schema.dim_date dt 			on its.fk_order_date = dt.id_date
	where 1=1
		and st.is_internal_marketplace_out = 1
		and st.store_name = 'KANUI'
		and dt.date_ between (sysdate - 64) and (sysdate - 7) --
		and paid_date is not null
		and datediff(day, its.order_date, its.paid_date) between 0 and 6 -- max relevant leadtime (>=0.9%)
	group by
	  dt.day_of_week 
	, cp.company_name 							
	--, st.store_name 					
	, datediff(day, its.order_date, its.paid_date)
	) aux1
join 
	(
	select 
	  dt.day_of_week  								as order_weekday
	, cp.company_name 								as company_name
	--, st.store_name									as store_name
	-- , datediff(day, its.order_date, its.paid_date) 	as processing_days
	, count(distinct(order_number))					as sample_total_qty
	from star_schema.fact_item_sold its
		left join star_schema.dim_company cp 		on its.fk_company = cp.id_company
		left join star_schema.dim_store st 			on st.fk_company = cp.id_company
		left join star_schema.dim_date dt 			on its.fk_order_date = dt.id_date
	where 1=1
		and st.is_internal_marketplace_out = 1
		and st.store_name = 'KANUI'
		and dt.date_ between (sysdate - 64) and (sysdate - 7)
		and paid_date is not null
		and datediff(day, its.order_date, its.paid_date) between 0 and 6 
	group by
	  dt.day_of_week 
	, cp.company_name 							
	--, st.store_name	
	-- , datediff(day, its.order_date, its.paid_date)
	) aux2   on aux1.order_weekday = aux2.order_weekday
				and aux1.company_name = aux2.company_name
				--and aux1.store_name = aux2.store_name
),

rule_inflow_date as (
select 
	aux1.paid_date 	as paid_date
	,aux2.inflow_date 	as inflow_date
from 
(select 
	date(dt.date_)	as paid_date
	,case 
		when (extract(d from dt.date_) <= 10) 
			then extract(y from dt.date_)::VARCHAR(10) || extract(mon from dt.date_)::VARCHAR(10)
		else extract(y from dateadd(month,1,dt.date_))::VARCHAR(10) 
				|| extract(mon from dateadd(month,1,dt.date_))::VARCHAR(10)
	 end as year_month
from star_schema.dim_date dt
where 1 
	and dt.iso_date between (sysdate - 95) and (sysdate + 300)) aux1

left join 

(select 
	extract(y from dt.date_)::VARCHAR(10) 
		||  extract(mon from dt.date_)::VARCHAR(10) as year_month 
	,max(dt.date_) as inflow_date
	from star_schema.dim_date dt
	where 1
		and dt.day_of_week not in (6,7)
		and dt.iso_date between (sysdate - 95) and (sysdate + 210)
	group by extract(y from dt.date_)::VARCHAR(10) 
			||  extract(mon from dt.date_)::VARCHAR(10)
) aux2 on aux1.year_month = aux2.year_month)


-------------------------------

(select 
	 aux.order_date 
	,aux.customer_paid_date
	,dtr.inflow_date			as inflow_date
	,aux.order_weekday
	,aux.company_name
	,'TRICAE' 					as store_name
	,aux.total_gmv_bef_cnc 		as total_gmv_bef_cnc
	,aux.total_gmv_aft_cnc 		as total_gmv_aft_cnc
	,aux.total_gmv_comm_aft_cnc	as total_gmv_comm_aft_cnc

from 
	(select 
		  dt.date_ as order_date
		, (dt.date_ + cf.customer_payment_leadtime_days) 	as customer_paid_date
		--, dtr.inflow_date									as inflow_date
		, dt.day_of_week  									as order_weekday
		, ma.store_name 									as company_name 
		--, 'TRICAE' 	as store_name
		, round(coalesce(ma.total_gmv_bef_cnc_bef_ret*ds.gmv_daily_share
			*st.tricae_share*(cf.expected_internal_mktplace_out_share),0),0) as total_gmv_bef_cnc
		, round(coalesce(ma.total_gmv_bef_cnc_bef_ret*ds.gmv_daily_share*st.tricae_share
			*(1- cn.cancellation_rate_tricae)*(cf.expected_internal_mktplace_out_share),0),0) as total_gmv_aft_cnc
		, round(coalesce(ma.total_gmv_bef_cnc_bef_ret*ds.gmv_daily_share*st.tricae_share
			*(1- cn.cancellation_rate_tricae)*(cf.expected_internal_mktplace_out_share)*(1-0.07),0),0) as total_gmv_comm_aft_cnc
	
	from targets.tgt_mkt_monthly_absolute ma
		join targets.tgt_mkt_daily_share ds  on ma.country_name = ds.country_name
		         and ma.store_name = ds.store_name
		         and ma.year = ds.year
		         and ma.month_of_year = ds.month_of_year
		join star_schema.dim_currency curr 	on curr.currency_code = ma.currency_code
		join star_schema.dim_date dt 		on cast(concat(concat(ma.year, ma.month_of_year), ds.day_of_month) as bigint) = dt.id_date
		join store st on ma.store_name = st.company_name
		join cnc cn on ma.store_name = cn.company_name
		join dbr_tricae_inflow cf 			on ma.store_name = cf.company_name
												and dt.day_of_week = cf.order_weekday

	where 1=1
		and ma.country_name = 'BRAZIL'
		and ma.store_name = 'DAFITI BRAZIL') aux

left join rule_inflow_date dtr 									on dtr.paid_date = aux.customer_paid_date

where 
	datediff(day, aux.order_date, coalesce(aux.customer_paid_date, aux.order_date)) >= 0 -- detected erros in DB, where order_date > paid_date. insignificant # occurrences.
)


UNION ALL


(select 
	 aux.order_date 
	,aux.customer_paid_date
	,dtr.inflow_date			as inflow_date
	,aux.order_weekday
	,aux.company_name
	,'KANUI' 					as store_name
	,aux.total_gmv_bef_cnc 		as total_gmv_bef_cnc
	,aux.total_gmv_aft_cnc 		as total_gmv_aft_cnc
	,aux.total_gmv_comm_aft_cnc	as total_gmv_comm_aft_cnc

from 
	(select 
		  dt.date_ as order_date
		, (dt.date_ + cf.customer_payment_leadtime_days) 	as customer_paid_date
		--, dtr.inflow_date									as inflow_date
		, dt.day_of_week  									as order_weekday
		, ma.store_name 									as company_name 
		--, 'KANUI' 	as store_name
		, round(coalesce(ma.total_gmv_bef_cnc_bef_ret*ds.gmv_daily_share
			*st.kanui_share*(cf.expected_internal_mktplace_out_share),0),0) as total_gmv_bef_cnc
		, round(coalesce(ma.total_gmv_bef_cnc_bef_ret*ds.gmv_daily_share*st.kanui_share
			*(1- cn.cancellation_rate_kanui)*(cf.expected_internal_mktplace_out_share),0),0) as total_gmv_aft_cnc
		, round(coalesce(ma.total_gmv_bef_cnc_bef_ret*ds.gmv_daily_share*st.kanui_share
			*(1- cn.cancellation_rate_kanui)*(cf.expected_internal_mktplace_out_share)*(1-0.07),0),0) as total_gmv_comm_aft_cnc
	from targets.tgt_mkt_monthly_absolute ma
		join targets.tgt_mkt_daily_share ds  on ma.country_name = ds.country_name
		         and ma.store_name = ds.store_name
		         and ma.year = ds.year
		         and ma.month_of_year = ds.month_of_year
		join star_schema.dim_currency curr 	on curr.currency_code = ma.currency_code
		join star_schema.dim_date dt 		on cast(concat(concat(ma.year, ma.month_of_year), ds.day_of_month) as bigint) = dt.id_date
		join store st on ma.store_name = st.company_name
		join cnc cn on ma.store_name = cn.company_name
		join dbr_tricae_inflow cf 			on ma.store_name = cf.company_name
												and dt.day_of_week = cf.order_weekday

	where 1=1
		and ma.country_name = 'BRAZIL'
		and ma.store_name = 'DAFITI BRAZIL') aux

left join rule_inflow_date dtr 									on dtr.paid_date = aux.customer_paid_date

where 
	datediff(day, aux.order_date, coalesce(aux.customer_paid_date, aux.order_date)) >= 0 -- detected erros in DB, where order_date > paid_date. insignificant # occurrences.
)