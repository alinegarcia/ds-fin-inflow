select 
  dt.date_ 											as order_date
, cp.company_name 									as company_name
, coalesce(py.payment_method_group, 'OTHER')		as payment_method_group
, sum(coalesce(its.total_gmv,0))					as total_gmv
-- , sum(coalesce(its.total_gmv, 0) 
-- 	- coalesce(its.refund_product_value, 0) 
-- 	- coalesce(its.refund_shipping_value, 0)
-- 	- coalesce(its.refund_gift_wrap_value, 0)) 		as total_gmv_less_refund_voucher_bef_cnc
-- , sum(case when its.is_paid = 1 
-- 		then (coalesce(its.total_gmv, 0) 
-- 		- coalesce(its.refund_product_value, 0) 
-- 		- coalesce(its.refund_shipping_value, 0) 
-- 		- coalesce(its.refund_gift_wrap_value, 0)) 
-- 		else 0 
-- 	  end)											as total_gmv_less_refund_voucher_aft_cnc
from star_schema.fact_item_sold its
	left join star_schema.dim_store st 			on its.fk_store = st.id_store
	left join star_schema.dim_company cp 		on its.fk_company = cp.id_company
	left join star_schema.dim_payment_method py on its.fk_payment_method = py.id_payment_method
	left join star_schema.dim_date dt 			on its.fk_order_date = dt.id_date
	left join mapping.map_company_store mcp 	on its.fk_company = mcp.fk_company and st.store_name = mcp.store_name
where 1=1
	and dt.date_ between (sysdate - 95) and (sysdate - 1)
	and cp.company_code in ('DBR','TRI','KAN')
	and its.is_invalid = 0
	and mcp.is_marketplace_out = 0 -- no marketplace out
group by
  dt.date_ 				 	
, cp.company_name 			
, coalesce(py.payment_method_group, 'OTHER')	
;